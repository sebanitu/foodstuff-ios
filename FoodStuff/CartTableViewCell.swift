//
//  CartTableViewCell.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 20.06.2021.
//

import UIKit
import RxSwift
import RxCocoa

final class CartTableViewCell: UITableViewCell, HasDisposeBag {

    @IBOutlet var itemNameLabel: UILabel!
    @IBOutlet var itemTotalPriceLabel: UILabel!
    @IBOutlet var incrementQuantityLabel: UILabel!
    @IBOutlet var decrementButton: UIButton!
    @IBOutlet var incrementButton: UIButton!
    @IBOutlet var containerView: UIView!
    
    var viewModelFactory: (CartCellViewModel.Inputs) -> CartCellViewModel = { _ in fatalError("Must provide factory function first.") }
    
    public static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configureViewModel() {
        setupReactiveExtensions()
    }
    
    private func setupReactiveExtensions() {
        let inputs = CartCellViewModel.Inputs(disposeBag: disposeBag,
                                              incrementTapped: incrementButton.rx.tap.asObservable(),
                                              decrementTapped: decrementButton.rx.tap.asObservable())
        
        let viewModel = viewModelFactory(inputs)
        let outputs = viewModel.outputs
        
        outputs
            .itemName
            .bind(to: itemNameLabel.rx.text)
            .disposed(by: disposeBag)
        
        outputs
            .itemQuantity
            .bind(to: incrementQuantityLabel.rx.text)
            .disposed(by: disposeBag)
        
        outputs
            .itemTotal
            .bind(to: itemTotalPriceLabel.rx.text)
            .disposed(by: disposeBag)
        
        outputs
            .itemRemoved
            .subscribe(onNext: { _ in
                UIDevice.sendHapticFeedback()
            }).disposed(by: disposeBag)
        
        outputs
            .itemAdded
            .subscribe(onNext: { _ in
                UIDevice.sendHapticFeedback()
            }).disposed(by: disposeBag)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.disposeBag = DisposeBag()
        self.itemNameLabel.text = ""
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.containerView.layer.masksToBounds = true
        self.containerView.layer.cornerRadius = 8
        self.containerView.addShadow()
        incrementButton.layer.borderWidth = 2
        incrementButton.layer.cornerRadius = incrementButton.frame.width / 2
        incrementButton.layer.borderColor = UIColor.accentColor.cgColor
        
        decrementButton.layer.borderWidth = 2
        decrementButton.layer.cornerRadius = incrementButton.frame.width / 2
        decrementButton.layer.borderColor = UIColor.accentColor.cgColor

     }

    

}
