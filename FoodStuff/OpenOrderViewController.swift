//
//  OpenOrderViewController.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 04.06.2021.
//

import UIKit
import RxSwift
import RxCocoa

final class OpenOrderViewController: UIViewController, HasDisposeBag {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusView: UIView!
    
    var viewModelFactory: (OpenOrderViewModel.Inputs) -> OpenOrderViewModel = { _ in fatalError("Must provide factory function first.") }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureSubviews()
        setupReactiveExtensions()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        statusView.layer.cornerRadius = 8
    }
    
    private func configureSubviews() {
     
    }
    
    private func setupReactiveExtensions() {
        let inputs = OpenOrderViewModel.Inputs(bag: disposeBag)
        let viewModel = viewModelFactory(inputs)
        let outputs = viewModel.outputs
        
        
        outputs
            .title
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)
        
        outputs
            .buttonColor
            .subscribe(onNext: {[weak self] color in
                guard let self = self else { return }
                self.statusView.backgroundColor = color
            }).disposed(by: disposeBag)
        
        outputs
            .buttonTitle
            .bind(to: statusLabel.rx.text)
            .disposed(by: disposeBag)
        
        
        outputs
            .transitionToCodeScan
            .subscribe(onNext: { _ in
                AppRouter.shared.switchToScanner()
            }).disposed(by: disposeBag)
    }
    

   

}
