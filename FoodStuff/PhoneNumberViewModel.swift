//
//  PhoneNumberViewModel.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 24.04.2021.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftExt
import FirebaseAuth
import PhoneNumberKit


final class  PhoneNumberViewModel {
 
    struct Inputs {
        let bag: DisposeBag
        let getCodeTapped: Observable<Void>
        let firstName: Observable<String?>
        let phoneNumber: Observable<String?>
    }

    struct Outputs {
        let getCodeButtonEnabled: Observable<Bool>
        let getCodeButtonLoading: Observable<Bool>
        let transitionToVerificationCode: Observable<Event<Void>>
        let firstName: Observable<String?>
        let phoneNumber: Observable<String?>
    }
    
    let outputs: Outputs
    
    init(inputs: Inputs) {
        
        let transitionToVerificationCode = PublishSubject<Event<Void>>()
        let phoneNumberKit = PhoneNumberKit()
        let regionString = "RO"
        let defaults = UserDefaults.standard
        

        let getCodeButtonEnabled = Observable.combineLatest(inputs.firstName, inputs.phoneNumber.unwrap()).map { firstName, phoneNumber -> Bool in

            let validPhoneNumber = phoneNumberKit.isValidPhoneNumber(phoneNumber, withRegion: regionString, ignoreType: true)
            
            defaults.setValue(firstName, forKey: "firstName")
            
            return (firstName?.count ?? 0 > 1) && validPhoneNumber
        }
        
        let getCodeTapped = inputs.getCodeTapped.share(replay: 1)
        
        let getCodeButtonLoading = getCodeTapped.mapTo(true).share(replay: 1)

        
        getCodeTapped
            .withLatestFrom(inputs.phoneNumber.unwrap())
            .flatMap({ (phoneNumber) -> Observable<Event<Void>> in
                do {
                    /// We parse the input phone number and create a PhoneNumber object
                    let phoneKitNumber = try phoneNumberKit.parse(phoneNumber, withRegion: regionString, ignoreType: true)
                    /// Format the phone number into the `e164 - The international public telecommunication numbering plan`
                    let formattedNumber = phoneNumberKit.format(phoneKitNumber, toType: .e164)
                    
                    defaults.setValue(formattedNumber, forKey: "phoneNumber")
                    /// Phone auth using the standardised phone number `ex: +40759389450`
                    return AuthManager.shared.initializePhoneAuth(phoneNumber: formattedNumber).materialize()
                    
                } catch (let error) {
                    print("⚠️ Error parsing input phone number. /nMake sure the number of expected characters is met or the number is a valid Romanian number. ERROR: \(error) ")
                    return .error(error)
                }
            }).bind(to: transitionToVerificationCode)
            .disposed(by: inputs.bag)
  
        outputs = Outputs(
            getCodeButtonEnabled: getCodeButtonEnabled,
            getCodeButtonLoading: getCodeButtonLoading,
            transitionToVerificationCode: transitionToVerificationCode,
            firstName: inputs.firstName,
            phoneNumber: inputs.phoneNumber
        )
    }
}
