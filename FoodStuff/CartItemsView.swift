//
//  CartItemsView.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 19.06.2021.
//

import UIKit
import RxGesture
import RxSwift
import RxCocoa
import Firebase

class CartItemsView: UIView {

    lazy var cartItemsCountLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .red
        label.textColor = .white
        label.font = .systemFont(ofSize: 8, weight: .semibold)
        label.layer.masksToBounds = true
        label.layer.cornerRadius = 6
        label.textAlignment = .center
        return label
    }()

    lazy var checkoutTitleLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 17, weight: .semibold)
        label.textAlignment = .center
        label.textColor = OrderManager.shared.pendingOrderItems.value.isEmpty ? .subtitleColor : .blue
        label.text = "Comandă"
        return label
     }()
    
    init(bag: DisposeBag, present: @escaping ((UIViewController) -> Void)) {
        super.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(checkoutTitleLabel)
        addSubview(cartItemsCountLabel)
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: 90),
            heightAnchor.constraint(equalToConstant: 32),
            
            checkoutTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            checkoutTitleLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            cartItemsCountLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            cartItemsCountLabel.topAnchor.constraint(equalTo: topAnchor),
            cartItemsCountLabel.widthAnchor.constraint(equalToConstant: 12),
            cartItemsCountLabel.heightAnchor.constraint(equalTo: cartItemsCountLabel.widthAnchor, multiplier: 1)
        ])

        let cartObserver = PublishSubject<Void>()
        
        self.rx.tapGesture().when(.recognized).mapTo(()).bind(to: cartObserver).disposed(by: bag)

        cartObserver
            .withLatestFrom(OrderManager.shared.pendingOrderItems)
            .subscribe(onNext: { orderItems in
                guard orderItems.count > 0 else { return }
                let controller = UIStoryboard.home.instantiateController(ofType: CartViewController.self)
                let viewModelFactory = { inputs -> CartViewModel in
                    return CartViewModel(inputs: inputs)
                }
                controller.viewModelFactory = viewModelFactory
                present(controller)
            })
            .disposed(by: bag)

        OrderManager.shared
            .pendingOrderItems
            .bind { [weak self] items in
                guard let self = self else { return }
                self.checkoutTitleLabel.textColor = items.isEmpty ? .subtitleColor : .accentColor
                self.cartItemsCountLabel.isHidden = items.isEmpty
                self.cartItemsCountLabel.text = "\(items.count)"
        }
        .disposed(by: bag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
