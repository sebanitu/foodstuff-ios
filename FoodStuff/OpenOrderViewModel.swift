//
//  OpenOrderViewModel.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 04.06.2021.
//

import Foundation
import RxSwift

class OpenOrderViewModel {
    
    struct Inputs {
        let bag: DisposeBag
    }
    
    struct Outputs {
        let title: Observable<String>
        let buttonColor: Observable<UIColor>
        let buttonTitle: Observable<String>
        let transitionToCodeScan: Observable<Void>
    }
    
    let outputs: Outputs
    
    init(inputs: Inputs, order: FSOrder) {
        
        let vendorAPI = VendorAPI()
        let orderAPI = OrderAPI()

        

        let title = vendorAPI
            .rx
            .getVendorLocationData(byId: order.locationId)
            .map { location -> String in
                return  "Ai o comandă activă la \(location.name)"
            }
        
        
        let orderStatus = orderAPI
            .rx
            .observeOrder(locationId: order.locationId, orderId: order.orderId)
            .map({$0.state})
            .share(replay: 1)
            
        let buttonTitle = orderStatus.map { status -> String in
            switch status {
            case .atTable: return "Comanda ta a fost servită"
            case .paid: return "Comanda a fost achitată"
            case .received: return "Comanda a fost primită"
            case .preparing: return "Comanda este în curs de preparare"
            }
        }
        
        let buttonColor = orderStatus.map { status -> UIColor in
            switch status {
            case .atTable: return .systemBlue
            case .paid: return .systemGreen
            case .received: return .systemYellow
            case .preparing: return .systemTeal
            }
        }
        
        let transitionToCodeScan = orderStatus.map({$0 == .paid}).filter({$0}).mapTo(())
        
        outputs = Outputs(title: title,
                          buttonColor: buttonColor,
                          buttonTitle: buttonTitle,
                          transitionToCodeScan: transitionToCodeScan)
    }
}
