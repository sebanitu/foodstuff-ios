//
//  HasCartButton.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 19.06.2021.
//

import Foundation
import UIKit
import RxSwift

protocol CartBarButtonItemConfiguration: UIViewController {
    func addCartBarButtonItem(present: @escaping ((UIViewController) -> Void))
    func setupCartBarButton()
}

extension CartBarButtonItemConfiguration where Self: UIViewController, Self: HasDisposeBag {
    func addCartBarButtonItem(present: @escaping ((UIViewController) -> Void)) {
        let view = CartItemsView(bag: disposeBag, present: present)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: view)
    }

    func setupCartBarButton() {
        addCartBarButtonItem(present: { [weak self] controller in
            // if it's a navigation controller present it, instead of pushing
            if controller is UINavigationController {
                controller.modalPresentationStyle = .fullScreen
                self?.present(controller, animated: true, completion: nil)
                return
            }
            guard let self = self, let nav = self.navigationController else { return }
            nav.pushViewController(controller, animated: true)
        })
    }
}

protocol HasCartBarButtonItem : CartBarButtonItemConfiguration {}
