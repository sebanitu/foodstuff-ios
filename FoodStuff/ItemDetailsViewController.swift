//
//  ItemDetailsViewController.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 20.06.2021.
//

import UIKit

final class ItemDetailsViewController: UIViewController, HasDisposeBag, HasCartBarButtonItem {
    
    @IBOutlet weak var itemDescriptionLabel: UILabel!
    @IBOutlet weak var itemPriceLabel: UILabel!
    @IBOutlet weak var addToCartButton: LoadingButton!
    
    var viewModelFactory: (ItemDetailsViewModel.Inputs) -> ItemDetailsViewModel = { _ in fatalError("Must provide factory function first.") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSubviews()
        setupReactiveExtensions()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addToCartButton.layer.cornerRadius = 8
        addToCartButton.addShadow()
    }
    
    private func configureSubviews() {
        setupCartBarButton()
    }
    
    private func setupReactiveExtensions() {
        let inputs = ItemDetailsViewModel.Inputs(bag: disposeBag,
                                                 addToCartButtonTapped: addToCartButton.rx.tap.asObservable())
        let viewModel = viewModelFactory(inputs)
        let outputs = viewModel.outputs
        
        outputs
            .itemName
            .bind(to: self.rx.title)
            .disposed(by: disposeBag)
        
        outputs
            .itemDescription
            .bind(to: itemDescriptionLabel.rx.text)
            .disposed(by: disposeBag)
        
        outputs
            .itemPrice
            .bind(to: itemPriceLabel.rx.text)
            .disposed(by: disposeBag)
        
        outputs
            .itemAddedToCart
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                //
            }).disposed(by: disposeBag)
    }
}
