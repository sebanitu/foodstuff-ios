//
//  AppRouter.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import UIKit

class AppRouter {
    
    let windowScene: UIWindowScene? = {
        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene else { return nil }
        return windowScene
    }()

    lazy var delegate: SceneDelegate? = {
        guard let delegate = windowScene?.delegate as? SceneDelegate else { return nil }
        return delegate
    }()

    lazy var window: UIWindow? = {
        guard let window = delegate?.window else { return nil }
        return window
    }()
    
    private init() {}
    
    public static let shared = AppRouter()
    
//    private var welcomeVC: UIViewController = {
//        let controller = UIStoryboard.auth.instantiateController(ofType: WelcomeViewController.self)
//        return controller
//    }()
    
    private var phoneNumberVC: UIViewController = {
        let controller = UIStoryboard.auth.instantiateController(ofType: PhoneNumberViewController.self)
        let viewModelFactory = { inputs -> PhoneNumberViewModel in
            return PhoneNumberViewModel(inputs: inputs)
        }
        controller.viewModelFactory = viewModelFactory
        return controller
    }()
    
    private var verificationCodeVC: UIViewController = {
        let controller = UIStoryboard.auth.instantiateController(ofType: VerificationCodeViewController.self)
        let viewModelFactory = { inputs -> VerificationCodeViewModel in
            return VerificationCodeViewModel(inputs)
        }
        controller.viewModelFactory = viewModelFactory
        return controller
    }()
    
    private var authNavController: UINavigationController = {
        let controller = UIStoryboard.auth.instantiateController(ofType: WelcomeViewController.self)
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.navigationBar.isHidden = true
        navigationController.navigationItem.hidesBackButton = true
        navigationController.interactivePopGestureRecognizer?.isEnabled = false

        return navigationController
    }()
    
    private var scannerVC: UIViewController = {
        let controller = UIStoryboard.home.instantiateController(ofType: CodeScanViewController.self)
        let viewModelFactory = { inputs -> CodeScanViewModel in
            return CodeScanViewModel(inputs: inputs)
        }
        controller.viewModelFactory = viewModelFactory
        return controller
    }()
    
    public func switchToAuthentication() {
        DispatchQueue.main.async {
            self.window?.rootViewController = self.authNavController
        }
    }
    
    public func switchToOpenOrder(withOrderData  order: FSOrder) {
        let controller = UIStoryboard.home.instantiateController(ofType: OpenOrderViewController.self)
        let viewModelFactory = { inputs -> OpenOrderViewModel in
            return OpenOrderViewModel(inputs: inputs, order: order)
        }
        controller.viewModelFactory = viewModelFactory        
        
        DispatchQueue.main.async {
            self.window?.rootViewController = controller
        }
    }
    
    public func transitionToVerificationCode() {
        DispatchQueue.main.async {
            self.authNavController.pushViewController(self.verificationCodeVC, animated: true)
        }
    }
    
    public func transitionToPhoneNumber() {
        DispatchQueue.main.async {
            self.authNavController.pushViewController(self.phoneNumberVC, animated: true)
        }
    }
    
    public func switchToScanner() {
        DispatchQueue.main.async {
            let controller = UIStoryboard.home.instantiateController(ofType: CodeScanViewController.self)
            let viewModelFactory = { inputs -> CodeScanViewModel in
                return CodeScanViewModel(inputs: inputs)
            }
            controller.viewModelFactory = viewModelFactory
            self.window?.rootViewController = controller
        }
    }
    
    public func switchToHome(withMenu menu: FSVendorMenu) {
        DispatchQueue.main.async {
            let menuCategoriesController = UIStoryboard.home.instantiateController(ofType: MenuCategoriesViewController.self)
            
            let viewModelFactory = { inputs -> MenuCategoriesViewModel in
                return MenuCategoriesViewModel(menu: menu, inputs: inputs)
            }
            menuCategoriesController.viewModelFactory = viewModelFactory
            
            let navigationController = UINavigationController(rootViewController: menuCategoriesController)
            
            self.window?.rootViewController = navigationController
        }
    }
}


