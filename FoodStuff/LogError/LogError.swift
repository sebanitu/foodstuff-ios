//
//  LogError.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation

enum LogError {}

// MARK: - Auth Errors
extension LogError {
    enum Auth: LocalizedError {
        case signInError
        case signUpError(String)
        case logOutError(String)
        case missingUser

        
        var errorDescription: String? {
            switch self {
            case .signInError:
                return "Sign in failed"
            case .signUpError(let phoneNumber):
                return "Signup is failed for phone number: - \(phoneNumber)"
            case .logOutError(let phoneNumber):
                return "Logout failed for phone number - \(phoneNumber)"
            case .missingUser:
                return "Missing Auth User object"
            }
        }
    }
}

extension LogError {
    enum LocationServices: LocalizedError {
        case locationServicesDisabled

        var errorDescription: String? {
            switch self {
            case .locationServicesDisabled: return "Location services disabled. Check Location Manager"
            }
        }
    }
}

// MARK: - Generic Errors

extension LogError {
    enum GenericError: LocalizedError, Equatable {
        case decodingFailure(String)
        case encodingFailure
        case emptyData

        var errorDescription: String? {
            switch self {
            case .decodingFailure(let decodingIssue):
                return decodingIssue

            case .encodingFailure:
                return "Encoding failed"

            case .emptyData:
                return "The call returned an empty value"
            }
        }
    }
}

// MARK: - User Errors
extension LogError {
    enum User: LocalizedError {
        case failedCreationOfUserInDatabse(Error)
        case failedEditingOfUserInDatabase(Error)
        case failedToSetNotificationToken(Error)
        case missingUserId
        case notForUser
        case missingAuthUser

        var errorDescription: String? {
            switch self {
            case .failedCreationOfUserInDatabse(let error):
                return "Creating the user object in the database failed. Firebase error: \(error.localizedDescription)"

            case .failedEditingOfUserInDatabase(let error):
                return "Editing the user object in the database failed. Firebase error: \(error.localizedDescription)"

            case .failedToSetNotificationToken(let error):
                return "Setting the notification token in the database failed. Firebase error: \(error.localizedDescription)"

            case .notForUser:
                return "Not for user"

            case .missingUserId:
                return "The id for this user could notbe found locally."
                
            case .missingAuthUser:
                return "We have no Auth User from Firebase, can't continue"
            }
        }
    }
}

