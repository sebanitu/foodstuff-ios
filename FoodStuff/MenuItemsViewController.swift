//
//  MenuItemsViewController.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 06.06.2021.
//

import UIKit
import RxSwift
import RxCocoa
import RxSwiftExt

final class MenuItemsViewController: UIViewController, HasCartBarButtonItem, HasDisposeBag {
    
    @IBOutlet weak var itemsTableView: UITableView!
    
        var viewModelFactory: (MenuItemsViewModel.Inputs) -> MenuItemsViewModel = { _ in fatalError("Must provide factory function first.") }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureSubviews()
        setupReactiveExtensions()
    }
    
    private func configureSubviews() {
        
        let cellNib = UINib(nibName: String(describing: MenuTableViewCell.self), bundle: nil)

        itemsTableView.register(cellNib,
                                     forCellReuseIdentifier: MenuTableViewCell.reuseIdentifier)
        
        itemsTableView.separatorStyle = .none
        itemsTableView.rowHeight = 87
    }
    
    private func setupReactiveExtensions() {
        
        setupCartBarButton()
        
        let inputs = MenuItemsViewModel.Inputs(bag: disposeBag)

        let viewModel = viewModelFactory(inputs)
        
        let outputs = viewModel.outputs
        
        outputs
            .categoryTitle
            .bind(to: self.rx.title)
            .disposed(by: disposeBag)
        
        outputs
            .items
            .bind(to: itemsTableView.rx.items(cellIdentifier: MenuTableViewCell.reuseIdentifier)) { index, item, cell in
                guard let cell = cell as? MenuTableViewCell else { return }
                cell.configure(withItem: item)
            }
            .disposed(by: disposeBag)
        
        itemsTableView
            .rx
            .modelSelected(FSMenuItem.self)
            .subscribe(onNext: { [weak self] menuItem in
                
                guard let self = self else { return }
                let itemDetailsVC = UIStoryboard.home.instantiateController(ofType: ItemDetailsViewController.self)
                let viewModelFactory = { inputs -> ItemDetailsViewModel in
                    return ItemDetailsViewModel(item: menuItem, inputs: inputs)
                }
                
                itemDetailsVC.viewModelFactory = viewModelFactory
                
                self.navigationController?.pushViewController(itemDetailsVC, animated: true)
                
            }).disposed(by: disposeBag)
    }
}
