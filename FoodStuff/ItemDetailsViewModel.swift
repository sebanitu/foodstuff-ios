//
//  ItemDetailsViewModel.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 20.06.2021.
//

import Foundation
import RxSwift

final class ItemDetailsViewModel {
    struct Inputs {
        let bag: DisposeBag
        let addToCartButtonTapped: Observable<Void>
    }
    struct Outputs {
        let itemDescription: Observable<String>
        let itemName: Observable<String>
        let itemAddedToCart: Observable<Void>
        let itemPrice: Observable<String>
    }
    
    public let outputs: Outputs
    
    init(item: FSMenuItem, inputs: Inputs) {
        let observableItem = Observable
            .just(item)
            .share(replay: 1)
        
        let itemDescription = observableItem.map({$0.description})
        
        let itemName = observableItem.map({$0.name})
        
        let itemPrice = observableItem.map({String("\($0.price) Lei")})
        
        let itemAddedToCart = inputs
            .addToCartButtonTapped
            .flatMap({OrderManager.shared.addItemToPendingOrder(item: item)})
            
            
        
        self.outputs = Outputs(itemDescription: itemDescription,
                               itemName: itemName,
                               itemAddedToCart: itemAddedToCart,
                               itemPrice: itemPrice)
    }
}
