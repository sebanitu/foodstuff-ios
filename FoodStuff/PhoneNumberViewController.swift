//
//  PhoneNumberViewController.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 24.04.2021.
//

import UIKit
import RxSwift
import RxCocoa

final class PhoneNumberViewController: UIViewController, HasDisposeBag {
    
    //MARK: Outlets
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: FSPhoneNumberTextField!
  
    @IBOutlet weak var verificationCodeButton: LoadingButton!
    
    var viewModelFactory: (PhoneNumberViewModel.Inputs) -> PhoneNumberViewModel = { _ in fatalError("Must provide factory function first.") }
    
    //MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupReactiveExtensions()
    }
    
    override func viewDidLayoutSubviews() {
        phoneNumberTextField.layer.borderWidth = 2
        phoneNumberTextField.layer.borderColor = UIColor.textfieldBorder.cgColor
        phoneNumberTextField.layer.cornerRadius = 8
        
        firstNameTextField .layer.borderWidth = 2
        firstNameTextField.layer.borderColor = UIColor.textfieldBorder.cgColor
        firstNameTextField.layer.cornerRadius = 8

        verificationCodeButton.layer.cornerRadius = 8
        super.viewDidLayoutSubviews()
        verificationCodeButton.addShadow()
    }
    
    private func setupReactiveExtensions() {
        let inputs = PhoneNumberViewModel.Inputs(
            bag: disposeBag,
            getCodeTapped: verificationCodeButton.rx.tap.asObservable(),
            firstName: firstNameTextField.rx.text.asObservable(),
            phoneNumber: phoneNumberTextField.rx.text.asObservable())

        let viewModel = viewModelFactory(inputs)

        let getCodeButtonEnabled = viewModel.outputs.getCodeButtonEnabled

        getCodeButtonEnabled
            .bind(to: verificationCodeButton.rx.isEnabled)
            .disposed(by: disposeBag)

        getCodeButtonEnabled
            .skip(until: viewModel.outputs.getCodeButtonLoading)
            .map({!$0})
            .distinctUntilChanged()
            .bind(to: verificationCodeButton.rx.isUserInteractionEnabled)
            .disposed(by: disposeBag)

        viewModel
            .outputs
            .getCodeButtonLoading
            .bind(to: verificationCodeButton.rx.isLoading)
            .disposed(by: disposeBag)
        
        viewModel
            .outputs
            .transitionToVerificationCode
            .take(1)
            .subscribe(onNext: { _ in
                AppRouter.shared.transitionToVerificationCode()
            }).disposed(by: disposeBag)
    }
    
    private func configureSubviews() {
        self.phoneNumberTextField.withPrefix = true
        self.phoneNumberTextField.keyboardType = .numberPad
    }

}
