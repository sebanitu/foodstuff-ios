//
//  CartViewController.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 20.06.2021.
//

import UIKit
import RxSwift
import RxCocoa

final class CartViewController: UIViewController, HasDisposeBag {

    @IBOutlet weak var cartItemsTableView: UITableView!
    
    @IBOutlet weak var cartTotalLabel: UILabel!
    @IBOutlet weak var placeOrderButton: LoadingButton!
    
    var viewModelFactory: (CartViewModel.Inputs) -> CartViewModel = { _ in fatalError("Must provide factory function first.") }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSubviews()
        setupReactiveExtensions()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        placeOrderButton.layer.cornerRadius = 8
        placeOrderButton.addShadow()
    }
    
    private func configureSubviews() {
        let cellNib = UINib(nibName: String(describing: CartTableViewCell.self), bundle: nil)

        cartItemsTableView.register(cellNib,
                                     forCellReuseIdentifier: CartTableViewCell.reuseIdentifier)
        
        cartItemsTableView.separatorStyle = .none
        cartItemsTableView.rowHeight = 100
    }
    
    private func setupReactiveExtensions() {
        let inputs = CartViewModel.Inputs(bag: disposeBag,
                                          placeOrderTapped: placeOrderButton.rx.tap.asObservable())
        
        let viewModel = viewModelFactory(inputs)
        
        let outputs = viewModel.outputs
        
        outputs
            .cartItems
            .bind(to: cartItemsTableView.rx.items(cellIdentifier: CartTableViewCell.reuseIdentifier)) { index, item, cell in
                guard let cell = cell as? CartTableViewCell else { return }
                
                let viewModelFactory = { inputs -> CartCellViewModel in
                    return CartCellViewModel(item: item, inputs: inputs)
                }
                cell.viewModelFactory = viewModelFactory
                
                cell.configureViewModel()
            }
            .disposed(by: disposeBag)
        
        outputs
            .currentOrderTotal
            .bind(to: cartTotalLabel.rx.text)
            .disposed(by: disposeBag)
        
        
        Observable.combineLatest(outputs.orderSubmitted, OrderManager.shared.lastSubmittedOrder.unwrap())
            .subscribe(onNext: { _, order in                
                AppRouter.shared.switchToOpenOrder(withOrderData: order)
            }).disposed(by: disposeBag)
        
        outputs
            .cartItems
            .map({$0.count > 0})
            .bind(to: placeOrderButton.rx.isEnabled)
            .disposed(by: disposeBag)
    }
}
