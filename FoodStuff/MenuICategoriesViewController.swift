//
//  MenuCategoriesViewController.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 06.06.2021.
//

import UIKit
import RxSwift
import RxCocoa
import RxSwiftExt

final class MenuCategoriesViewController: UIViewController, HasCartBarButtonItem, HasDisposeBag {
    
    
    @IBOutlet weak var categoriesTableView: UITableView!
    
    var viewModelFactory: (MenuCategoriesViewModel.Inputs) -> MenuCategoriesViewModel = { _ in fatalError("Must provide factory function first.") }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureSubviews()
        setupReactiveExtensions()
    }
    
    private func configureSubviews() {
        
        self.title = "Meniu"
        let navigationBar = navigationController?.navigationBar
        navigationBar?.prefersLargeTitles = true
        navigationBar?.barTintColor = .backgroundColor
        navigationBar?.tintColor = .accentColor
        
        let cellNib = UINib(nibName: String(describing: MenuTableViewCell.self), bundle: nil)

        categoriesTableView.register(cellNib,
                                     forCellReuseIdentifier: MenuTableViewCell.reuseIdentifier)
        
        categoriesTableView.separatorStyle = .none
        categoriesTableView.rowHeight = 87
    }
    
    private func setupReactiveExtensions() {
        
        setupCartBarButton()
        
        let inputs = MenuCategoriesViewModel.Inputs(bag: disposeBag)

        let viewModel = viewModelFactory(inputs)
        
        let outputs = viewModel.outputs
        
        outputs
            .categories
            .bind(to: categoriesTableView.rx.items(cellIdentifier: MenuTableViewCell.reuseIdentifier)) { index, category, cell in
                guard let cell = cell as? MenuTableViewCell else { return }
                cell.configure(withCategory: category)
            }
            .disposed(by: disposeBag)
        
        categoriesTableView
            .rx
            .modelSelected(FSMenuCategory.self)
            .subscribe(onNext: { [weak self] category in
                guard let self = self, category.items != nil else { return }
                
                
                let menuItemsVC = UIStoryboard.home.instantiateController(ofType: MenuItemsViewController.self)
              
                
                let viewModelFactory = { inputs -> MenuItemsViewModel in
                    return MenuItemsViewModel(menu: category, inputs: inputs)
                }
                menuItemsVC.viewModelFactory = viewModelFactory
                
                self.navigationController?.pushViewController(menuItemsVC, animated: true)
                
            }).disposed(by: disposeBag)
    }
}
