//
//  LoadingButton.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa


class LoadingButton: UIButton {


    private var indicatorStyle: UIActivityIndicatorView.Style = {
        return .medium
    }()

    private lazy var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(style: indicatorStyle)
    private var title: String = ""

    func setActivityIndicatorViewStyle(_ style: UIActivityIndicatorView.Style) {
        activityIndicator.style = style
    }

    required init?(coder: NSCoder) {
        self.activityIndicatorStyle = indicatorStyle
        super.init(coder: coder)
        configureSubivews()
    }

    override init(frame: CGRect) {
        self.activityIndicatorStyle = indicatorStyle
        super.init(frame: frame)
        configureSubivews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 8
        self.addShadow()
    }

    private func configureSubivews() {
        activityIndicator.hidesWhenStopped = true
           title = self.titleLabel?.text ?? ""

           addSubview(activityIndicator)
           activityIndicator.translatesAutoresizingMaskIntoConstraints = false
           NSLayoutConstraint.activate([
               activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor),
               activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor)
           ])
    }

    var isLoading: Bool = false {
        willSet {
            switch newValue {
            case true:
                activityIndicator.startAnimating()
                setTitle("", for: .normal)
                isUserInteractionEnabled = false
            case false:
                activityIndicator.stopAnimating()
                setTitle(title, for: .normal)
                isUserInteractionEnabled = true
            }
        }
    }

    var activityIndicatorStyle: UIActivityIndicatorView.Style {
        didSet {
            self.activityIndicator.style = activityIndicatorStyle
        }
    }

    var activityIndicatorColor: UIColor = .gray {
        didSet {
            self.activityIndicator.color = activityIndicatorColor
        }
    }

    var enabledBackgroundColor: UIColor = .accentColor {
        didSet {
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }

    var disabledBackgroundColor: UIColor = .systemGray {
        didSet {
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }

    override var isEnabled: Bool {
        willSet {
            switch newValue {
            case true:
                self.setBackgroundColor(color: enabledBackgroundColor, forState: .normal)
                self.backgroundColor = enabledBackgroundColor
            case false:
                self.setBackgroundColor(color: disabledBackgroundColor, forState: .disabled)
            }
        }
    }

}

extension Reactive where Base: LoadingButton {

    /// Bindable sink for `loading` property.
    var isLoading: Binder<Bool> {
        return Binder(self.base) { (target, isLoading) in
            target.isLoading = isLoading
        }
    }
    
}
