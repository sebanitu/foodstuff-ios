//
//  UIView+Customization.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import UIKit

extension UIView {
    func applyGradient(gradientColors: [CGColor]) {
        let gradient = CAGradientLayer()
        gradient.colors = gradientColors
        gradient.frame = self.bounds
        self.layer.addSublayer(gradient)
    }

    func applyDtGradient()  {
        applyGradient(gradientColors: [UIColor.white.cgColor,
                                       UIColor.white.cgColor,
                                       UIColor(red: 250/255, green: 190/255, blue: 191/255, alpha: 1).cgColor])
    }

    func roundCorners(_ corners: CACornerMask? = nil, radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        if let maskedCorners = corners {
            self.layer.maskedCorners = maskedCorners
        }
        self.layer.cornerRadius = radius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }

    func addShadow(radius: CGFloat = 10, shadowOpacity: Float = 0.08, color: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), size: CGSize = CGSize(width: 0, height: 2)) {
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: radius).cgPath
        self.layer.shouldRasterize = false
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = size
    }
    
    func addCustomShadow() {
        self.addShadow(radius: 10, shadowOpacity: 0.3, color: .accentColor, size: CGSize(width: 3, height: 3))
    }
    
    func roundCorners(corners: UIRectCorner, radius: Int = 8) {
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: corners,
                                     cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}

// MARK: - Initialization From Nib

extension UIView {
    class func fromNib<T: UIView>() -> T {
        guard let view = Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)?[0] as? T else { fatalError("Could not initialise from nib") }
        return view
    }
}

// MARK: - Subviews Operations
extension UIView {

    func removeAllSubviews() {
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }

    func allSubviews() -> Set<UIView> {
        var childSubviews = Set(subviews)
        for subview in subviews {
            childSubviews = childSubviews.union(subview.allSubviews())
        }
        return childSubviews
    }

}

// MARK: - Subviews Operations
extension UIView {

    func pinToEdges(of view: UIView) {
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: view.leadingAnchor),
            topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            trailingAnchor.constraint(equalTo: view.trailingAnchor),
            bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }

}
// MARK: - Fade in / out Animations
extension UIView {
    func fadeIn(duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)
    }

    func fadeOut(duration: TimeInterval = 1.0, delay: TimeInterval = 3.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}

