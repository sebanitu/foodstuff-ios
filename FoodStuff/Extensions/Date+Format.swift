//
//  Date+Format.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation

let DateISO8601Format = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
