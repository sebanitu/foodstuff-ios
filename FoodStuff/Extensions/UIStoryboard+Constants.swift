//
//  UIStoryboard+Constants.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 24.04.2021.
//

import UIKit

extension UIStoryboard {
    @nonobjc static let loading = UIStoryboard(name: "Loading", bundle: nil)
    @nonobjc static let auth = UIStoryboard(name: "Auth", bundle: nil)
    @nonobjc static let home = UIStoryboard(name: "Home", bundle: nil)

    func instantiateController<T>(ofType type: T.Type) -> T {
        
        guard let controller = instantiateViewController(withIdentifier: String(describing: T.self)) as? T else {
            fatalError("Could not instantiate view controller")
        }

        return controller
    }
}

