//
//  PhoneNumberTextField.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import UIKit
import PhoneNumberKit

class FSPhoneNumberTextField: PhoneNumberTextField {

    private let placeholderColor: UIColor = .lightGray

    override var defaultRegion: String {
            get {
                return "RO"
            }
            set {} // exists for backward compatibility
        }

    override var placeholder: String? {
        didSet {
            let attributes = [ NSAttributedString.Key.foregroundColor: placeholderColor ]
            attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: attributes)
        }
    }

    private let padding = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 5)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
