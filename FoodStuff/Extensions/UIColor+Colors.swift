//
//  UIColor+Colors.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import UIKit

extension UIColor {
    
    static var accentColor: UIColor {
        return UIColor(named: "RedAccent")!
    }
    
    static var backgroundColor: UIColor {
        return UIColor(named: "Background")!
    }
    
    static var subtitleColor: UIColor {
        return UIColor(named: "Subtitle")!
    }
    
    static var titleColor: UIColor {
        return UIColor(named: "Title")!
    }
    
    static var textfieldBorder: UIColor {
        return UIColor(named: "TextfieldBorder")!
    }
    
    static var textfieldPlaceholder: UIColor {
        return UIColor(named: "TextfieldPlaceholder")!
    }
    
    static var textfieldText: UIColor {
        return UIColor(named: "TextfieldText")!
    }
}
