//
//  Sequence+Hashable.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 20.06.2021.
//

import Foundation

// Filters array for unique items
extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var seen: Set<Iterator.Element> = []
        return filter { seen.insert($0).inserted }
    }
}
