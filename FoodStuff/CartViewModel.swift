//
//  CartViewModel.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 20.06.2021.
//

import Foundation
import RxSwift
import RxSwiftExt

final class CartViewModel {
    
    struct Inputs {
        let bag: DisposeBag
        let placeOrderTapped: Observable<Void>
    }
    
    struct Outputs {
        let cartItems: Observable<[FSMenuItem]>
        let currentOrderTotal: Observable<String>
        let orderSubmitted: Observable<Void>
        let placedOrder: Observable<FSOrder?>
    }
    
    public let outputs: Outputs
    
    init(inputs: Inputs) {
        
        let orderAPI = OrderAPI()
        let userAPI = UserAPI()
        
        let cartItems = OrderManager
            .shared
            .pendingOrderItems
            .map({$0.unique()})
            .share(replay: 1)
        
        let currentOrderTotal = OrderManager
            .shared
            .currentTotal
            .map({String("Total: \($0) Lei")})
        
        
        let placedOrder = inputs
            .placeOrderTapped
            .flatMap { _ in
                OrderManager.shared.prepareOrderForSubmission()
            }
        
        
        let orderSubmitted =
            placedOrder
            .flatMap { order -> Observable<FSOrder> in
                guard let order = order else { return .empty() }
                return orderAPI.rx.submit(order: order)
            }.flatMap { order in
                return userAPI.rx.setUserOrder(orderId: order.orderId, locationId: order.locationId, tableId: order.tableId)
            }
        
        self.outputs = Outputs(cartItems: cartItems,
                               currentOrderTotal: currentOrderTotal,
                               orderSubmitted: orderSubmitted,
                               placedOrder: placedOrder)
    }
}
