//
//  CodeScanViewModel.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation
import AVFoundation
import RxSwift
import RxCocoa
import RxSwiftExt

final class CodeScanViewModel {
    
    struct Inputs {
        let bag: DisposeBag
        let grantCameraAccessTapped: Observable<Void>
        let scanResult: Observable<FSScanResult>
    }
    
    struct Outputs {
        let welcomeText: Observable<String>
        let startCaptureSession: Observable<Void>
        let cameraAccessAuthorized: Observable<Bool>
        let testLabelData: Observable<String>
        let codeScanned: Observable<FSVendorMenu>
    }
    
    private let cameraAccess = PublishSubject<AVAuthorizationStatus>()
    private let showQRCamera = PublishSubject<Bool>()
   // private let startCameraSession = PublishSubject<Void>()
    private let cameraAccessAuthorized = PublishSubject<Bool>()
    private let vendorTableData = PublishSubject<String>()

    let outputs: Outputs
    
    init(inputs: Inputs) {
        
        let container = Interface.container
        let vendorAPI = container.resolve(VendorAPI.self)!
        let userAPI = container.resolve(UserAPI.self)!
                
        OrderManager.shared.pendingOrderItems.accept([])

        
        let welcomeText = CurrentUser
            .sharedInstance
            .user
            .unwrap()
            .flatMap({userAPI.rx.getUser(byId: $0.id)})
            .map({"Salut, \($0.firstName)!"})
        
        let scanResult = inputs.scanResult.share(replay: 1)

        
        let cameraAccessAuthorized =
            cameraAccess
            .map({$0 == .authorized})
            .share(replay: 1)
        
        let startCameraSession = cameraAccessAuthorized
            .filter({$0})
            .mapTo(())
           
        
        let locationData =
            scanResult
            .distinctUntilChanged()
            .flatMap({
                vendorAPI.rx.getVendorLocationData(byId: $0.locationId)
            })
        
        let menuData = locationData
            .flatMap({vendorAPI.rx.getVendorMenuData(byId: $0.menuId)})
            .take(1)
        
        let tableData = scanResult
            .flatMap({vendorAPI.rx.getVendorTableData($0.tableId, fromLocation: $0.locationId)})
        
        tableData.subscribe(onNext: { table in
            OrderManager.shared.currentTable = table
        }).disposed(by: inputs.bag)
        
        scanResult.subscribe(onNext: { scanResult in
            OrderManager.shared.currentVendorLocationId = scanResult.locationId
        }).disposed(by: inputs.bag)
        
        
    
        outputs = Outputs(welcomeText: welcomeText,
                          startCaptureSession: startCameraSession,
                          cameraAccessAuthorized: cameraAccessAuthorized,
                          testLabelData: vendorTableData,
                          codeScanned: menuData)
        
        checkCameraAcces()
            .bind(to: cameraAccess)
            .disposed(by: inputs.bag)
                
        
       
        

        
        let vendorData = PublishSubject<FSVendor>()
            scanResult
            .distinctUntilChanged()
            .flatMap({vendorAPI.rx.getVendorData(byId: $0.vendorId)})
                .bind(to: vendorData)
                .disposed(by: inputs.bag)
        
       

        
        
        inputs.grantCameraAccessTapped
            .flatMap { _  -> Observable<Bool> in
                return self.requestCameraAccess()
            }
            .flatMap { _ -> Observable<AVAuthorizationStatus> in
                return self.checkCameraAcces()
            }.bind(to: cameraAccess)
            .disposed(by: inputs.bag)
    }
    
    private func checkCameraAcces() -> Observable<AVAuthorizationStatus> {
        return Observable.create { observer in
            let status = AVCaptureDevice.authorizationStatus(for: .video)
            observer.onNext(status)
            observer.onCompleted()
            return Disposables.create()
        }
    }
    
    private func requestCameraAccess() -> Observable<Bool> {
        return Observable.create { observer in
            AVCaptureDevice.requestAccess(for: .video) { granted in
                observer.onNext(granted)
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }
}
