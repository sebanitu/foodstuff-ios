//
//  Interface.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 24.04.2021.
//

import Foundation
import Swinject

class Interface {
    
    static let container: Container = {
        let container = Container()
        container.register(FirebaseService.self) { _ in FirebaseService() }
            .inObjectScope(.container)
        container.register(FrameworkService.self) { _ in FrameworkService() }
            .inObjectScope(.container)
        container.register(UserAPI.self) { _ in UserAPI()}
            .inObjectScope(.container)
        container.register(VendorAPI.self) { _ in VendorAPI()}
            .inObjectScope(.container)
        container.register(OrderAPI.self) { _ in OrderAPI()}
            .inObjectScope(.container)
        return container
    }()
}
