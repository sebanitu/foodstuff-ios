//
//  FSEncoder.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation
import CodableFirebase

class FSEncoder: FirebaseEncoder {
    public override init() {
        super.init()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateISO8601Format
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let encodingStrategy = DateEncodingStrategy.formatted(dateFormatter)
        self.dateEncodingStrategy = encodingStrategy
    }
}

