//
//  FSDecoder.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation
import CodableFirebase

class FSDecoder: FirebaseDecoder {
    public override init() {
        super.init()
        self.dateDecodingStrategy = .iso8601
    }

    public init(customFormat: String) {
        super.init()

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = customFormat

        let decodingStrategy = DateDecodingStrategy.formatted(dateFormatter)
        self.dateDecodingStrategy = decodingStrategy
    }
}
