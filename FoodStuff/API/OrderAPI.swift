//
//  OrderAPI.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 20.06.2021.
//

import Foundation

class OrderAPI: NSObject {
    
    func submit(order: FSOrder, completion: @escaping (Result<FSOrder, Error>) -> Void) {
        guard let currentLocationId = OrderManager.shared.currentVendorLocationId else {
            return
            
        }
        
        guard let encodedOrder = try? FSEncoder().encode(order) else {
            return
            
        }
        
        FSDatabaseReference.Orders.referenceForVendorOpenOrders(currentLocationId).ref.child(order.orderId).setValue(encodedOrder) { error, _ in
            if let error = error {
                completion(.failure(error))
                return
            }
            completion(.success(order))
        }
    }
    
    func observeLocationOrders(locationId: String, completion: @escaping (Result<[FSOrder], Error>) -> Void) {
        let ref = FSDatabaseReference.Orders.referenceForVendorOrders(locationId).ref
        ref.removeAllObservers()
        
        ref.observe(.value) { snapshot in
            guard snapshot.exists(), let value = snapshot.value else {
                completion(.failure(LogError.GenericError.emptyData))
                return
            }
                        
            do {
                if let dict = value as? [String: [String: Any]] {
                    let orders = dict.map { $0.1 }
                    let decodedOrderData = try FSDecoder().decode([FSOrder].self, from: orders)
                completion(.success(decodedOrderData))
                } else {
                    completion(.success([]))
                }
            } catch let error {
                completion(.failure(error))
            }
        }
    }
    
    func observeOrder(locationId: String, orderId: String, completion: @escaping (Result<FSOrder, Error>) -> Void) {
        let ref = FSDatabaseReference.Orders.referenceForSpecificOrder(orderId, locationId: locationId).ref
        ref.removeAllObservers()
        
        ref.observe(.value) { snapshot in
            guard snapshot.exists(), let value = snapshot.value else {
                print("Ref is \(ref.debugDescription)")
                completion(.failure(LogError.GenericError.emptyData))
                return
            }
                        
            do {
                let decodedOrderData = try FSDecoder().decode(FSOrder.self, from: value)
                completion(.success(decodedOrderData))
            } catch let error {
                completion(.failure(error))
            }
        }
    }
    
    func getOrder(locationId: String, orderId: String, completion: @escaping (Result<FSOrder?, Error>) -> Void) {
        let ref = FSDatabaseReference.Orders.referenceForSpecificOrder(orderId, locationId: locationId).ref
        
        ref.observeSingleEvent(of: .value) { snapshot in
            guard snapshot.exists(), let value = snapshot.value else {
                completion(.success(nil))
                return
            }
                        
            do {
                let decodedOrderData = try FSDecoder().decode(FSOrder.self, from: value)
                completion(.success(decodedOrderData))
            } catch let error {
                completion(.failure(error))
            }
        }
    }
    
    func updateOrderState(locationId: String, orderId: String, status: FSOrderState, completion: @escaping (Result<Void, Error>) -> Void) {
        
        guard let updateData = try? FSEncoder().encode(status) as? [AnyHashable: Any] else {
            completion(.failure(LogError.GenericError.encodingFailure))
            return
        }
        
        FSDatabaseReference.Orders.referenceForSpecificOrder(orderId, locationId: locationId).ref
            .updateChildValues(updateData) { error,_ in
                if let error = error {
                    completion(.failure(LogError.User.failedCreationOfUserInDatabse(error)))
                    return
                }
                completion(.success(()))
            }
    }
    
    private func removeOrder(order: FSOrder, completion: @escaping (Result<Void, Error>) -> Void) {
        
        guard let encodedOrder = try? FSEncoder().encode(order) else {
            return
        }
        
        FSDatabaseReference.Orders.referenceForVendorArchivedOrders(order.locationId).ref.child(order.orderId).setValue(encodedOrder) { error, _ in
            if let error = error {
                completion(.failure(error))
                return
            }
            completion(.success(()))
        }
    }
    
    private func removeUserOrder(order: FSOrder, completion: @escaping (Result<Void, Error>) -> Void) {
        FSDatabaseReference.User.openOrderReferenceForUserWithId(order.userId).ref.removeValue()
            completion(.success(()))
        }
    
    func archiveOrder(order: FSOrder, completion: @escaping (Result<Void, Error>) -> Void) {
        removeOrder(order: order) { result in
            switch result {
            case .success(()):
                self.removeUserOrder(order: order) { result in
                    switch result {
                    case .success(()): FSDatabaseReference.Orders.referenceForSpecificOrder(order.orderId, locationId: order.locationId).ref
                        .removeValue()
                        completion(.success(()))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
                completion(.success(()))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}



