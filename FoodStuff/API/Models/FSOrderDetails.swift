//
//  FSOrderDetails.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 19.06.2021.
//

import Foundation

struct FSOrderDetails {
    let table: FSTable?
    let orderItems: [Int:FSMenuItem]
}
