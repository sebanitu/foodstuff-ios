//
//  SignUpRequestModel.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation

struct SignupUserRequestModel: Codable {
    let id: String
    let firstName: String?
    let phoneNumber: String?
    let isAdmin: Bool
}
