//
//  FSTable.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 09.05.2021.
//

import Foundation

struct FSTable: Codable {
    let id: String
    let tableNumber: Int
}
