//
//  FSOrder.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 20.06.2021.
//

import Foundation

enum FSOrderState: String, Codable, Hashable {
    case received = "RECEIVED"
    case preparing = "PREPARING"
    case atTable = "AT_TABLE"
    case paid = "PAID"
}

struct FSOrderItem: Codable, Hashable {
    let name: String
    let quantity: Int
    let itemTotal: Double
    let unitPrice: Double
}

struct FSOrder: Codable, Hashable {
    let orderId: String
    let locationId: String
    let vendorId: String
    let user: String
    let userId: String
    let tableNumber: Int
    let tableId: String
    let orderTotal: Double
    let state: FSOrderState
    let items: [FSOrderItem]
    let receivedAt: String?
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(tableId)
    }
}
