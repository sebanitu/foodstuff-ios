//
//  FSScanResults.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 15.05.2021.
//

import Foundation

struct FSScanResult: Equatable {
    let vendorId: String
    let locationId: String
    let tableId: String
    
    static func ==(lhs: FSScanResult, rhs: FSScanResult) -> Bool {
        return lhs.vendorId == rhs.vendorId &&
            lhs.locationId == rhs.locationId &&
            lhs.tableId == rhs.tableId
    }
}
