//
//  FSUser.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation

struct FSUserOrder: Codable {
    let orderId: String
    let locationId: String
    let tableId: String
}

struct FSUser: Codable {
    var id: String
    var firstName: String
    var phoneNumber: String
    var openOrder: FSUserOrder?
}


