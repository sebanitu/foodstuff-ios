//
//  FSVendor.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation

struct FSVendor: Codable {
    let id: String
    let name: String
    let address: String
}

extension FSVendor: Identifiable {
    
}

