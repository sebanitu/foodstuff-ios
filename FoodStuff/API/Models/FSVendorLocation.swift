//
//  FSVendor.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 15.05.2021.
//

import Foundation

struct FSVendorTable: Codable {
    let id: String
    let tableNumber: Int
}

struct FSVendorLocation: Codable {
    let id: String
    let menuId: String
    let name: String
    let address: String
}

struct FSMenuItem: Codable, Hashable, Equatable {
    let name: String
    let price: Double
    let description: String
    let imageURL: String?
    
    var hashValue: Int {
        var hasher = Hasher()
        hash(into: &hasher)
        return hasher.finalize()
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(description)
    }

    static func ==(lhs: FSMenuItem, rhs: FSMenuItem) -> Bool {
        return rhs.name == lhs.name &&
            lhs.description == rhs.description &&
            lhs.price == rhs.price
    }
}

struct FSMenuCategory: Codable {
    let name: String
    let description: String
    let imageURL: String
    let items: [FSMenuItem]?
}

struct FSVendorMenu: Codable {
    let name: String
    let categories: [FSMenuCategory]
}


