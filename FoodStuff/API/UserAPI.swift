//
//  UserAPI.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation
import Firebase

class UserAPI: NSObject {
    
    func createDBUser(user: SignupUserRequestModel, completion: @escaping (Result<Void, Error>) -> Void) {
        guard let updateData = try? FSEncoder().encode(user) as? [AnyHashable: Any] else {
            completion(.failure(LogError.GenericError.encodingFailure))
            return
        }

        FSDatabaseReference.User.referenceForUserWithId(user.id)
            .ref
            .updateChildValues(updateData) { error,_ in
                if let error = error {
                    completion(.failure(LogError.User.failedCreationOfUserInDatabse(error)))
                    return
                }
                completion(.success(()))
        }
    }
    
    func getUserById(userId: String, completion: @escaping (Result<FSUser, Error>) -> Void) {
        FSDatabaseReference.User.referenceForUserWithId(userId).ref.observeSingleEvent(of: .value) { snapshot in
            guard snapshot.exists(), let value = snapshot.value else {
                completion(.failure(LogError.GenericError.emptyData))
                return
            }
            do {
                let decodedUserData = try FSDecoder().decode(FSUser.self, from: value)
                completion(.success(decodedUserData))
            } catch let error {
                completion(.failure(error))
            }
        }
    }
    
    func setUserOrder(orderId: String, locationId: String, tableId: String,  completion: @escaping (Result<Void, Error>) -> Void) {

        guard var user = CurrentUser.sharedInstance.user.value else { return }
        
        user.openOrder = FSUserOrder(orderId: orderId, locationId: locationId, tableId: tableId)
        
        guard let updateData = try? FSEncoder().encode(user) as? [AnyHashable: Any] else {
            completion(.failure(LogError.GenericError.encodingFailure))
            return
        }
        
        FSDatabaseReference.User.referenceForUserWithId(user.id)
            .ref
            .updateChildValues(updateData) { error,_ in
                if let error = error {
                    completion(.failure(LogError.User.failedCreationOfUserInDatabse(error)))
                    return
                }
                completion(.success(()))
        }
    }
}
