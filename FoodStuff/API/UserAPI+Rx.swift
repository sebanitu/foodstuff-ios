//
//  UserAPI+Rx.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation
import RxCocoa
import RxSwift

extension Reactive where Base: UserAPI {
    func createDatabaseUser(withModel userModel: SignupUserRequestModel) -> Observable<Void> {
        return Observable.create { observer in
            self.base.createDBUser(user: userModel) { result in
                switch result {
                case .success(_):
                    observer.onNext(())
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    func getUser(byId userId: String) -> Observable<FSUser> {
        return Observable.create { observer in
            self.base.getUserById(userId: userId) { result in
                switch result {
                case .success(let user):
                    observer.onNext(user)
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    func setUserOrder(orderId: String, locationId: String, tableId: String) -> Observable<Void> {
        return Observable.create { observer in
            self.base.setUserOrder(orderId: orderId, locationId: locationId, tableId: tableId) { result in
                switch result {
                case .success():
                    observer.onNext(())
                    observer.onCompleted()
                    
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
}
