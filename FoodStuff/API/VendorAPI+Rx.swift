//
//  VendorAPI+Rx.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation
import RxCocoa
import RxSwift

extension Reactive where Base: VendorAPI {
    func getVendorData(byId vendorId: String) -> Observable<FSVendor> {
        return Observable.create { observer in
            self.base.getVendorData(vendorId: vendorId) { result in
                switch result {
                case .success(let vendor):
                    observer.onNext(vendor)
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    func getVendorLocationData(byId locationId: String) -> Observable<FSVendorLocation> {
        return Observable.create { observer in
            self.base.getLocationData(locationId: locationId) { result in
                switch result {
                case .success(let location):
                    observer.onNext(location)
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    func getVendorTableData(_ tableId: String, fromLocation locationId: String) -> Observable<FSVendorTable> {
        return Observable.create { observer in
            self.base.getTableData(locationId: locationId, tableId: tableId) { result in
                switch result {
                case .success(let table):
                    observer.onNext(table)
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    func getVendorMenuData(byId menuId: String) -> Observable<FSVendorMenu> {
        return Observable.create { observer in
            self.base.getMenuData(menuId: menuId){ result in
                switch result {
                case .success(let menu):
                    observer.onNext(menu)
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
}
