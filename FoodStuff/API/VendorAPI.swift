//
//  VendorAPI.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation

class VendorAPI: NSObject {
    
    func getVendorData(vendorId: String, completion: @escaping (Result<FSVendor, Error>) -> Void) {
        FSDatabaseReference.Vendors.referenceForVendorWithId(vendorId).ref.observeSingleEvent(of: .value) { snapshot in
            guard snapshot.exists(), let value = snapshot.value else {
                completion(.failure(LogError.GenericError.emptyData))
                return
            }
            do {
                let decodedVendorData = try FSDecoder().decode(FSVendor.self, from: value)
                completion(.success(decodedVendorData))
            } catch let error {
                completion(.failure(error))
            }
        }
    }
    
    func getLocationData(locationId: String, completion: @escaping (Result<FSVendorLocation, Error>) -> Void) {
        FSDatabaseReference.Location.referenceForLocationWithId(locationId).ref.observeSingleEvent(of: .value) { snapshot in
            
            guard snapshot.exists(), let value = snapshot.value else {
                completion(.failure(LogError.GenericError.emptyData))
                return
            }
            do {
                let decodedVendorData = try FSDecoder().decode(FSVendorLocation.self, from: value)
                completion(.success(decodedVendorData))
            } catch let error {
                completion(.failure(error))
            }
        }
    }
    
    func getTableData(locationId: String, tableId: String, completion: @escaping (Result<FSVendorTable, Error>) -> Void) {
        FSDatabaseReference.Location.referenceForTableInLocation(locationId: locationId, tableId: tableId).ref.observeSingleEvent(of: .value) { snapshot in
            
            guard snapshot.exists(), let value = snapshot.value else {
                completion(.failure(LogError.GenericError.emptyData))
                return
            }
            do {
                let decodedVendorData = try FSDecoder().decode(FSVendorTable.self, from: value)
                completion(.success(decodedVendorData))
            } catch let error {
                completion(.failure(error))
            }
        }
    }
    
    func getMenuData(menuId: String, completion: @escaping (Result<FSVendorMenu, Error>) -> Void) {
        FSDatabaseReference.Menus.referenceForMenuWithId(menuId).ref.observeSingleEvent(of: .value) { snapshot in
            
            guard snapshot.exists(), let value = snapshot.value else {
                completion(.failure(LogError.GenericError.emptyData))
                return
            }
            do {
                let decodedVendorData = try FSDecoder().decode(FSVendorMenu.self, from: value)
                completion(.success(decodedVendorData))
            } catch let error {
                completion(.failure(error))
            }
        }
    }
}
