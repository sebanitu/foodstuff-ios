//
//  OrderAPI+Rx.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 20.06.2021.
//

import Foundation
import RxSwift

extension Reactive where Base: OrderAPI {

    func submit(order: FSOrder) -> Observable<FSOrder> {
        return Observable.create { observer in
            base.submit(order: order) { result in
                switch result {
                case .success(let order):
                    observer.onNext(order)
                    observer.onCompleted()
            
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    func observeOrder(locationId: String, orderId: String) -> Observable<FSOrder> {
        return Observable.create { observer in
            
            base.observeOrder(locationId: locationId, orderId: orderId) { result in
                switch result {
                case .success(let order):
                    observer.onNext(order)
                    
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    func getOrder(locationId: String, orderId: String) -> Observable<FSOrder?> {
        return Observable.create { observer in
            base.getOrder(locationId: locationId, orderId: orderId) { result in
                switch result {
                case .success(let order):
                    observer.onNext(order)
                    observer.onCompleted()
                    
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
    func archiveOrder(order: FSOrder) -> Observable<Void> {
        return Observable.create { observer in
            base.archiveOrder(order: order) { result in
                switch result {
                case .success():
                    observer.onNext(())
                    observer.onCompleted()
                    
                case .failure(let error):
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
}



