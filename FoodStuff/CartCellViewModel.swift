//
//  CartCellViewModel.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 20.06.2021.
//

import Foundation
import RxSwift
import RxSwiftExt

final class CartCellViewModel {
    struct Inputs {
        let disposeBag: DisposeBag
        let incrementTapped: Observable<Void>
        let decrementTapped: Observable<Void>
    }
    struct Outputs {
        let itemName: Observable<String>
        let itemTotal: Observable<String>
        let itemQuantity: Observable<String>
        let itemRemoved: Observable<Void>
        let itemAdded: Observable<Void>
    }
    
    public let outputs: Outputs
    
    init(item: FSMenuItem, inputs: Inputs) {
        let observableItem = Observable.just(item)
        
        let orderManager = OrderManager.shared
        
        let itemQuantity = orderManager
            .pendingOrderItems
            .map({$0.filter({$0.name == item.name})})
            .map({$0.count})
            
        
        let itemTotalString = Observable
            .combineLatest(observableItem, itemQuantity)
            .map({$0.price * Double($1)})
            .map({String($0) + " Lei"})
        
        let itemRemoved  = inputs
            .decrementTapped
            .withLatestFrom(itemQuantity)
            .take(while: { $0 >= 1})
            .map({ _ in
                orderManager.pendingOrderItems.value.firstIndex(where: {$0.name == item.name})!
            })
            .flatMap({orderManager.removeItemFromPendingOrder(index: $0)})
        
        let itemAdded  = inputs
            .incrementTapped
            .withLatestFrom(observableItem)
            .flatMap({orderManager.addItemToPendingOrder(item: $0)})
            

        let itemName = observableItem.map({$0.name})
        
        self.outputs = Outputs(itemName: itemName,
                               itemTotal: itemTotalString,
                               itemQuantity: itemQuantity.map({String($0)}),
                               itemRemoved: itemRemoved,
                               itemAdded: itemAdded)
    }
}
