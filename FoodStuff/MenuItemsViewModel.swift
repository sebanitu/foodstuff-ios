//
//  MenuItemsViewModel.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 06.06.2021.
//

import Foundation
import RxSwift


final class MenuItemsViewModel {
    
    struct Inputs {
        let bag: DisposeBag
    }
    struct Outputs {
        let categoryTitle: Observable<String>
        let items: Observable<[FSMenuItem]>
    }
    
    let outputs: Outputs
    
    init(menu: FSMenuCategory?, inputs: Inputs) {
        
        let menuData = Observable
            .just(menu)
            .share(replay: 1)
        
        let items = menuData
            .unwrap()
            .map({$0.items})
            .unwrap()
        
        let categoryTitle = menuData.unwrap().map({$0.name})
        
        self.outputs = Outputs(categoryTitle: categoryTitle,
                               items: items)
    }
}
