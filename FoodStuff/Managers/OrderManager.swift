//
//  OrderManager.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 19.06.2021.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftExt

class OrderManager {
    public static let shared = OrderManager()

    var onOpenOrdersUpdated: ((FSOrderDetails) -> ())? = nil
    var pendingOrderItems = BehaviorRelay<[FSMenuItem]>(value: [])

    var openOrderData: FSOrderDetails? {
        didSet {
            guard let orderData = openOrderData else {
                openOrderData = nil
                return
            }
            onOpenOrdersUpdated?(orderData)
        }
    }
    
    var currentTotal: Observable<Double> {
        return pendingOrderItems.map { menuItems -> Double in
            return menuItems.map({$0.price}).reduce(0.0, +)
        }
    }
        
    var currentTable: FSVendorTable?
    
    var currentVendorLocationId: String?
    
    let lastSubmittedOrder = BehaviorRelay<FSOrder?>(value: nil)
    
    func prepareOrderForSubmission() -> Observable<FSOrder?> {
        
        let countedPendingItems = pendingOrderItems.map { pendingItems -> [FSMenuItem : Int] in
            let countedItems = pendingItems.reduce(into: [:]) { counts, item in
                counts[item, default: 0] += 1
            }
            
            return countedItems
        }
        
        return Observable.combineLatest(countedPendingItems, currentTotal)
            .map { [weak self] countedItems, total -> FSOrder? in
                var orderItems: [FSOrderItem] = []
                guard let self = self,
                      let currentUser = CurrentUser.sharedInstance.user.value,
                      let locationId = self.currentVendorLocationId,
                      let currentTable = self.currentTable else { return nil }
                
                
                
                countedItems.keys.forEach { item in
                    guard let itemQuantity = countedItems[item] else { return }
                    let itemTotal = item.price * Double(itemQuantity)
                    
                    let orderItem = FSOrderItem(name: item.name, quantity: itemQuantity, itemTotal: itemTotal, unitPrice: item.price)
                    
                    orderItems.append(orderItem)
                }
                
                    let now = Date()

                   let formatter = DateFormatter()

                   formatter.timeZone = TimeZone.current

                   formatter.dateFormat = "yyyy-MM-dd HH:mm"

                   let dateString = formatter.string(from: now)
                
                let firstName = currentUser.firstName
                
                let userId = currentUser.id
                
                let order = FSOrder(orderId: UUID().uuidString, locationId: locationId, vendorId: "", user: firstName, userId: userId, tableNumber: currentTable.tableNumber, tableId: currentTable.id, orderTotal: total, state: .received, items: orderItems, receivedAt: dateString)
                
                self.lastSubmittedOrder.accept(order)
                
                return order
            }
    }
    


    func addItemToPendingOrder(item: FSMenuItem) {
        var existingItems = pendingOrderItems.value
        existingItems.append(item)
        pendingOrderItems.accept(existingItems)
    }
    
    func addItemToPendingOrder(item: FSMenuItem) -> Observable<Void> {
        return Observable.create { [weak self] observer in
            guard let self = self else { return Disposables.create() }
            var existingItems = self.pendingOrderItems.value
            existingItems.append(item)
            self.pendingOrderItems.accept(existingItems)
            observer.onNext(())
            observer.onCompleted()
            return Disposables.create()
        }
    }

    func removeItemFromPendingOrder(index: Int){
        var existingItems = pendingOrderItems.value
        existingItems.remove(at: index)
        pendingOrderItems.accept(existingItems)
    }
    
    func removeItemFromPendingOrder(index: Int) -> Observable<Void> {
        return Observable.create { [weak self] observer in
            guard let self = self else { return Disposables.create() }
            var existingItems = self.pendingOrderItems.value
            existingItems.remove(at: index)
            self.pendingOrderItems.accept(existingItems)
            observer.onNext(())
            observer.onCompleted()
            return Disposables.create()
        }
    }

    func resetPendingOrder(){
        pendingOrderItems.accept([])
    }
}
