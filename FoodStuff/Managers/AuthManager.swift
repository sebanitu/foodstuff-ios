//
//  AuthManager.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 24.04.2021.
//

import Foundation
import Firebase
import RxSwift


class AuthManager {
    static let shared = AuthManager()
    
    func initializePhoneAuth(phoneNumber: String) -> Observable<Void> {

        return Observable.create { observer -> Disposable in
            PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { verificationId, error in
                if let error = error {
                    observer.onError(error)
                }
                UserDefaults.standard.set(verificationId, forKey: "authVerificationID")
                observer.onNext(())
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }
    
    func loginWithVerificationCode(verificationCode: String) -> Observable<Void>{

        return Observable.create { observer -> Disposable in
            try? Auth.auth().signOut()

            let verificationID = UserDefaults.standard.string(forKey: "authVerificationID") ?? ""
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID, verificationCode: verificationCode)

            Auth.auth().signIn(with: credential) { user, error in
                if let error = error {
                    observer.onError(error)
                    return
                }
                observer.onNext(())
                observer.onCompleted()
            }

            return Disposables.create()
        }
    }
    
    func logOut() {
        guard Auth.auth().currentUser != nil else { return }
        do {
            try Auth.auth().signOut()
            AppRouter.shared.switchToAuthentication()
        } catch _ as NSError {
            
        }
    }
}
