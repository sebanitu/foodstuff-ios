//
//  CurrentUser.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation
import RxSwift
import RxCocoa
import FirebaseAuth


fileprivate let storedUserKey = "kStoredUserKey"

class CurrentUser: NSObject, HasDisposeBag {
    static let sharedInstance: CurrentUser = CurrentUser()
    let userAPI: UserAPI

    static var storedUser: FSUser? {
        get {
            guard let decoded = UserDefaults.standard.object(forKey: storedUserKey) as? Data else {
                return nil
            }
            let user = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? FSUser
            return user
        }
        set {
            guard let value = newValue else { return }
            let encodedData = try? NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: false)
            let userDefaults = UserDefaults.standard
            userDefaults.set(encodedData, forKey: storedUserKey)
        }
    }
    
    let user = BehaviorRelay<FSUser?>(value: nil)


    var profileImageURL: URL? {
        return Auth.auth().currentUser?.photoURL
    }
    
    override init() {
        let container = Interface.container
        self.userAPI = container.resolve(UserAPI.self)!
        super.init()
    }
    

    /// Call that sets the data for the current user singleton
    public func updateCurrentUser() -> Observable<FSUser> {
        guard let user = Auth.auth().currentUser else {
            return Observable.error(LogError.Auth.signInError)
        }

        return userAPI.rx.getUser(byId: user.uid)
            .do(onNext: { userDetails in
                CurrentUser.storedUser = userDetails
                CurrentUser.sharedInstance.user.accept(userDetails)
            }, onError: { error in
                AppRouter.shared.switchToAuthentication()
            })
            .materialize()
            .elements()
    }
    

//    #if !APPCLIP
//    public func uploadImage(image: UIImage?) -> Observable<StorageMetadata> {
//        guard let user = Auth.auth().currentUser else { fatalError("Shouln't be here if not logged in ") }
//        guard let data = image?.jpegData(compressionQuality: 0.0) else { return .error(DTError()) }
//
//        /// Create a reference to the file you want to upload
//        let profileImageRef = Storage.storage().reference().child("profile_images/\(user.uid).png")
//
//        let metadata = StorageMetadata()
//        metadata.contentType = "image/png"
//
//        /// Upload the file to the path `"profile_images/UID.png"`
//        return profileImageRef.rx.putData(data, metadata: metadata)
//    }
//
//    public func downloadProfileImageURL() -> Observable<URL> {
//        guard let user = Auth.auth().currentUser else { fatalError("Shouln't be here if not logged in ") }
//
//        let profileImageRef = Storage.storage().reference().child("profile_images/\(user.uid).png")
//
//        return profileImageRef.rx.downloadURL()
//    }
//
//    #endif
//
//    public func updateProfileImage(_ url: URL?) -> Observable<Void> {
//        guard let user = Auth.auth().currentUser else { fatalError("Shoudln't be here if not logged in") }
//
//        return Observable.create { observer -> Disposable in
//            let changeRequest = user.createProfileChangeRequest()
//            changeRequest.photoURL = url
//            changeRequest.commitChanges { error in
//                if let error = error {
//                    observer.onError(error)
//                }
//                observer.onNext(())
//                observer.onCompleted()
//            }
//            return Disposables.create()
//        }
//    }
}
