//
//  MenuCategoriesViewModel.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 06.06.2021.
//

import Foundation
import RxSwift


final class MenuCategoriesViewModel {
    
    struct Inputs {
        let bag: DisposeBag
    }
    struct Outputs {
        let categories: Observable<[FSMenuCategory]>
    }
    
    let outputs: Outputs
    
    init(menu: FSVendorMenu?, inputs: Inputs) {
        
        let menuData = Observable.just(menu)
        
        let categories = menuData.unwrap().map({$0.categories})
        
        self.outputs = Outputs(categories: categories)
    }
}
