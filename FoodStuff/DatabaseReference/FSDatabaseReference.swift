//
//  FSDatabaseReference.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation
import Firebase

enum FSDatabaseReference {
    case baseReference
    
    static var basePath: DatabaseReference {
            return Database.database().reference()
    }
    
    static var connected: DatabaseReference {
        return Database.database().reference(withPath: ".info/connected")
    }
    
    var ref: DatabaseReference {
        return FSDatabaseReference.basePath
    }
}
