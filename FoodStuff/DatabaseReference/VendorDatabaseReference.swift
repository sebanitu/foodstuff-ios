//
//  VendorDatabaseReference.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation
import Firebase

extension FSDatabaseReference {
    
    enum Vendors {
        
        /// Upper most lever containing all the vendors data
        case baseVendorReference
        
        case referenceForVendorWithId(_ id: String)
        
        var ref: DatabaseReference {
            switch self {
            
            case .baseVendorReference:
                return FSDatabaseReference.baseReference.ref
                    .child("vendors")
                
            case .referenceForVendorWithId(let vendorId):
                return FSDatabaseReference.Vendors.baseVendorReference.ref.child(vendorId)
            }
        }
    }
}
