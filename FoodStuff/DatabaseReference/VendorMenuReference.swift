//
//  VendorLocationReference.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 17.05.2021.
//

import Foundation
import Firebase

extension FSDatabaseReference {
    
    enum Menus {
        
        /// Upper most lever containing all the menus data
        case baseMenusReference
        
        case referenceForMenuWithId(_ id: String)
        
        var ref: DatabaseReference {
            switch self {
            
            case .baseMenusReference:
                return FSDatabaseReference.baseReference.ref
                    .child("menus")
                
            case .referenceForMenuWithId(let menuId):
                return FSDatabaseReference.Menus.baseMenusReference.ref.child(menuId)
            }
        }
    }
}
