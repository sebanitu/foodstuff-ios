//
//  VendorLocationReference.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 17.05.2021.
//

import Foundation
import Firebase

extension FSDatabaseReference {
    
    enum Orders {
        
        /// Upper most lever containing all the locations data
        case baseOrderReference
        
        case referenceForVendorOrders(_ id: String)
        
        case referenceForVendorOpenOrders(_ id: String)

        case referenceForVendorArchivedOrders(_ id: String)

        case referenceForSpecificOrder(_ id: String, locationId: String)
        
        
        var ref: DatabaseReference {
            switch self {
            
            case .baseOrderReference:
                return FSDatabaseReference.baseReference.ref
                    .child("orders")
                
            case .referenceForVendorOrders(let locationId):
                return FSDatabaseReference.Orders.baseOrderReference.ref.child(locationId)
                
                
            case .referenceForVendorOpenOrders(let locationId):
                return FSDatabaseReference.Orders.referenceForVendorOrders(locationId).ref.child("open")
                
            case .referenceForVendorArchivedOrders(let locationId):
                return FSDatabaseReference.Orders.referenceForVendorOrders(locationId).ref.child("archived")
            
            case .referenceForSpecificOrder(let orderId, let locationId):
                return FSDatabaseReference.Orders.referenceForVendorOpenOrders(locationId).ref.child(orderId)
            }
            
        }
    }
}
