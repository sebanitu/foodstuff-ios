//
//  VendorLocationReference.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 17.05.2021.
//

import Foundation
import Firebase

extension FSDatabaseReference {
    
    enum Location {
        
        /// Upper most lever containing all the locations data
        case baseLocationReference
        
        case referenceForLocationWithId(_ id: String)
        
        case referenceForTableInLocation(locationId: String, tableId: String)
        
        var ref: DatabaseReference {
            switch self {
            
            case .baseLocationReference:
                return FSDatabaseReference.baseReference.ref
                    .child("locations")
                
            case .referenceForLocationWithId(let locationId):
               
                return FSDatabaseReference.Location.baseLocationReference.ref.child(locationId)
                
            case .referenceForTableInLocation(let locationId, let tableId):
                return FSDatabaseReference.Location.baseLocationReference.ref.child(locationId).child("tables").child(tableId)
            }
        }
    }
}
