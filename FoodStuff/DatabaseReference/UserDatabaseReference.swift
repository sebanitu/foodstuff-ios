//
//  UserDatabaseReference.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation
import Firebase

extension FSDatabaseReference {
    
    enum User {
        
        /// Upper most lever containing all the users data
        case baseUsersReference
        
        case referenceForUserWithId(_ id: String)
        
        case firstNameRefereceForUserWithId(_ id: String)
        
        case phoneNumberReferenceForUserWithId(_ id: String)
        
        case openOrderReferenceForUserWithId(_ id: String)

        
        var ref: DatabaseReference {
            switch self {
            
            case .baseUsersReference:
                return FSDatabaseReference.baseReference.ref
                    .child("users")
                
            case .referenceForUserWithId(let id):
                return FSDatabaseReference.User.baseUsersReference.ref
                    .child(id)
                
            case .firstNameRefereceForUserWithId(let id):
                return FSDatabaseReference.User.referenceForUserWithId(id).ref
                    .child("firstName")
                
            case .phoneNumberReferenceForUserWithId(let id):
                return FSDatabaseReference.User.referenceForUserWithId(id).ref
                    .child("phoneNumber")
                
            case .openOrderReferenceForUserWithId(let id):
                return FSDatabaseReference.User.referenceForUserWithId(id).ref
                    .child("openOrder")
            }
        }
    }
}

