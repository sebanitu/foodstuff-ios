//
//  MenuTableViewCell.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 06.06.2021.
//

import UIKit

final class MenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var categoryImageView: UIImageView!
    
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var categoryDescriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
        
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public var category: FSMenuCategory?
    
    
    public static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    public func configure(withCategory category: FSMenuCategory) {
        self.priceLabel.isHidden = true
        self.category = category
        self.categoryNameLabel.text = category.name
        self.categoryDescriptionLabel.text = category.description
        self.categoryImageView.loadImageUsingCacheWithUrlString(urlString: category.imageURL)
    }
    
    public func configure(withItem item: FSMenuItem) {
        self.priceLabel.isHidden = false
        self.categoryNameLabel.text = item.name
        self.categoryDescriptionLabel.text = item.description
        self.categoryImageView.loadImageUsingCacheWithUrlString(urlString: item.imageURL)
        self.priceLabel.text = String(item.price) + " " + "Lei"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.containerView.layer.masksToBounds = true
        self.containerView.layer.cornerRadius = 8
        self.categoryImageView.roundCorners(corners: [.topLeft, .bottomLeft], radius: 8)
        self.containerView.addShadow()
     }
}
