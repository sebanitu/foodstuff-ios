//
//  VerificationCodeViewModel.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import Foundation
import RxSwift
import RxCocoa
import FirebaseAuth
import Firebase

final class VerificationCodeViewModel {
    
    struct Inputs {
        let bag: DisposeBag
        let verificationCode: Observable<String?>
        let signInTapped: Observable<Void>
    }
    
    struct Outputs {
        let signInButtonEnabled: Observable<Bool>
        let signInButtonLoading: Observable<Bool>
        let signedIn: Observable<Void>
    }
    
    let outputs: Outputs
    
    init(_ inputs: Inputs) {
        
        let defaults = UserDefaults.standard
        let firstName = Observable.just(defaults.string(forKey: "firstName"))
        let phoneNumber = Observable.just(defaults.string(forKey: "phoneNumber"))
        let container = Interface.container
        let userAPI = container.resolve(UserAPI.self)!
        let signedIn = PublishSubject<Void>()
        
        let signInButtonTapped = inputs.signInTapped.share(replay: 1)
        
        let loginWithVerificationCode = signInButtonTapped
            .withLatestFrom(inputs.verificationCode)
            .unwrap()
            .flatMapLatest({ (verificationCode) -> Observable<Event<Void>> in
                return AuthManager.shared.loginWithVerificationCode(verificationCode: verificationCode).materialize()
            })
                
         Observable.combineLatest(loginWithVerificationCode, firstName, phoneNumber)
            .flatMap({ (loginEvent, firstName, phoneNumber) -> Observable<Void> in
                switch loginEvent {
                case .error(let error):
                    print("Auth Sign In Error: \(error)")
                    return .empty()
                case .next():
                    guard let currentUser = Auth.auth().currentUser else {
                        return .empty()
                    }
                    
                    let userRequestModel = SignupUserRequestModel(id: currentUser.uid, firstName: firstName, phoneNumber: phoneNumber, isAdmin: false)
                    
                    return userAPI.rx.createDatabaseUser(withModel: userRequestModel)
                    
                default:
                    return .empty()
                }
            }).bind(to: signedIn)
            .disposed(by: inputs.bag)
        
        let signInButtonLoading = signInButtonTapped.mapTo(true)
       
        let signInButtonEnabled =
            inputs
            .verificationCode
            .unwrap()
            .map { codeString -> Bool in
                let codeRegex = "^[0-9]{6}$"
                let codeTest =  NSPredicate(format:"SELF MATCHES %@", codeRegex)
                return codeTest.evaluate(with: codeString)
            }
        
        outputs = Outputs(signInButtonEnabled: signInButtonEnabled, signInButtonLoading: signInButtonLoading, signedIn: signedIn)
    }
}
