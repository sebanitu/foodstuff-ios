//
//  FirebaseService.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 24.04.2021.
//

import Foundation
import Firebase
import RxSwift
import RxCocoa
import Swinject


final class FirebaseService: NSObject, HasDisposeBag {
    
    public func setup() -> Observable<Void> {
       
        return Observable.create { [weak self] observer -> Disposable in
            guard let self = self else { return Disposables.create()}
            FirebaseApp.configure()
            self.setupAuth()
            self.signOutOldUser()
            observer.onNext(())
            return Disposables.create()
        }
    }
}

private extension FirebaseService {

    func setupAuth() {
//        guard let currentUser = Auth.auth().currentUser else { return }
//        if currentUser.hasGoneThroughOnboarding?.boolValue ?? false && !UIDevice.isSimulator(), !currentUser.isAnonymous {
//            PushNotificationService.shared.setupPushNotifications()
        }
    
    /// This deletes any firebase auth reference if the user has not previously logged out. Otherwise, it will remain in a weird state
    /// There's pretty much no other way of doing this, because Firebase keeps a reference of the auth in the keychain, so that's not deleted when reinstalling
    /// At least until they release something in the SDK itself
    func signOutOldUser() {
//        guard UserDefaults.standard.value(forKey: FirebaseKeys.appFirstTimeOpened) == nil else { return }
//        UserDefaults.standard.set(true, forKey: FirebaseKeys.appFirstTimeOpened)
//        try? Auth.auth().signOut()
    }
}


