//
//  FrameworkService.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 24.04.2021.
//

import UIKit
import UserNotifications
import RxSwift
import RxCocoa
import Swinject

//
// This class manages the Frameworks at a high level
//


final class FrameworkService: NSObject, HasDisposeBag {
    let firebaseService: FirebaseService

    override init() {
        // Swinject container resolution
        let container = Interface.container
        self.firebaseService = container.resolve(FirebaseService.self)!
    }
}

private extension FrameworkService {
    var appLaunched: Observable<Void> {
        // setup other services
        return Observable.create { [weak self] observer -> Disposable in
            guard let self = self else { return Disposables.create() }
            return self.firebaseService.setup()
                .subscribe(onNext: { _ in
                    observer.onNext(())
                    observer.onCompleted()
                })
        }
    }
}

// MARK: - UIApplicationDelegate

extension FrameworkService: UIApplicationDelegate {
   @nonobjc func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Observable<Void> {
        return self.appLaunched
    }
}
