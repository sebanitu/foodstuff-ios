//
//  WelcomeViewController.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import UIKit
import RxSwift
import RxCocoa

class WelcomeViewController: UIViewController, HasDisposeBag {

    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        nextButton
            .rx
            .tap
            .subscribe(onNext: { _ in
                AppRouter.shared.transitionToPhoneNumber()
            }).disposed(by: disposeBag)
    }
    
    override func viewDidLayoutSubviews() {
        nextButton.layer.cornerRadius = 8
        super.viewDidLayoutSubviews()
        nextButton.addShadow()
    }
}
