//
//  AppDelegate.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 24.04.2021.
//

import UIKit
import RxSwift
import Reachability
import RxReachability
import FirebaseAuth

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var frameworkService: FrameworkService?
    var reachability: Reachability?
    var finishedLoading: Observable<Void>?
    
    private func loadModules() {
        let container = Interface.container
        self.frameworkService = container.resolve(FrameworkService.self)!
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        reachability = try? Reachability()
        try? reachability?.startNotifier()
        loadModules()
        
        guard let frameworkService = frameworkService else {
            return true
        }
        
        finishedLoading = reachability?.rx
              .isReachable
              .filter({$0 == true})
              .distinctUntilChanged()
              .flatMap({ (_) -> Observable<Void> in
                  frameworkService.application(application, didFinishLaunchingWithOptions: launchOptions)
              })
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

