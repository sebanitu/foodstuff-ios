//
//  SceneDelegate.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 24.04.2021.
//

import UIKit
import FirebaseAuth
import RxSwift

class SceneDelegate: UIResponder, UIWindowSceneDelegate, HasDisposeBag {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        let loadingVC = UIStoryboard.loading.instantiateController(ofType: LoadingViewController.self)
        window.rootViewController = loadingVC
        self.window = window
        window.makeKeyAndVisible()
        
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate, let finishedLoading = appDelegate.finishedLoading else { return }
        
        finishedLoading
            .flatMapLatest { _ -> Observable<Event<FSUser>> in
                return CurrentUser.sharedInstance.updateCurrentUser().materialize()
                    .observe(on: MainScheduler.asyncInstance)
            }.subscribe(onNext: {userEvent in
                switch userEvent {
                case .next(let user):
                    
                    if let openOrder = user.openOrder {
                        let orderAPI = OrderAPI()
                        
                        orderAPI.getOrder(locationId: openOrder.locationId, orderId: openOrder.orderId) { result in
                            switch result {
                            case .success(let orderData):
                                guard let orderData = orderData else { return }
                                AppRouter.shared.switchToOpenOrder(withOrderData: orderData)
                                return
                            case .failure(_):
                                return
                            }
                        }
                        
                    } else {
                        AppRouter.shared.switchToScanner()
                    }
                case .error(_):
                    AppRouter.shared.switchToAuthentication()
                default:
                    return
                }
            }).disposed(by: disposeBag)
        
        
        
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        for urlContext in URLContexts {
            let url = urlContext.url
            Auth.auth().canHandle(url)
        }
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    
}

