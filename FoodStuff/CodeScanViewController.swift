//
//  CodeScanViewController.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import UIKit
import AVFoundation
import SnapKit
import RxSwift
import RxCocoa
import RxGesture

class CodeScanViewController: UIViewController, HasDisposeBag {
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var cameraPermissionStackView: UIStackView!
    @IBOutlet weak var accessCameraButton: LoadingButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    private var captureSession = AVCaptureSession()
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    private var audioPlayer: AVAudioPlayer?
    private let scannedVendorData = PublishSubject<FSScanResult>()
    
    
    private let qrCodeView: UIView = {
        let view = UIView()
        view.backgroundColor = .backgroundColor
        view.isHidden = false
        view.clipsToBounds = true
        return view
    }()
    
    private let qrImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(named: "QRCode")
        imageView.contentMode = .scaleAspectFill
        imageView.tintColor = .accentColor.withAlphaComponent(0.3)
        return imageView
    }()
    
    var viewModelFactory: (CodeScanViewModel.Inputs) -> CodeScanViewModel = { _ in fatalError("Must provide factory function first.") }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureSubviews()
        setupScanner()
        setupReactiveExtensions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)

    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.logoutButton.layer.cornerRadius = 8
        self.logoutButton.addShadow()
    }
    
    private func configureSubviews() {
        self.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(qrCodeView)
        qrCodeView.addSubview(qrImageView)
        
        accessCameraButton.isHidden = true

        qrCodeView.snp.makeConstraints { make in
            make.width.equalToSuperview().multipliedBy(0.75)
            make.height.equalTo(qrCodeView.snp.width)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        qrImageView.snp.makeConstraints({$0.leading.trailing.top.bottom.equalToSuperview()})
        
        self.captureSession.startRunning()
        
       
    }
    
   
    private func setupReactiveExtensions() {
        
        let inputs = CodeScanViewModel.Inputs(bag: disposeBag,
                                              grantCameraAccessTapped: self.accessCameraButton.rx.tap.asObservable(),
                                              scanResult: scannedVendorData)

        let viewModel = viewModelFactory(inputs)
        
        let outputs = viewModel.outputs
        
        
        outputs
            .codeScanned
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] menuData in
                guard let self = self else { return }
                UIDevice.sendHapticFeedback()
                self.playScanSound()
                AppRouter.shared.switchToHome(withMenu: menuData)
            }).disposed(by: disposeBag)
        
        outputs
            .welcomeText
            .bind(to: welcomeLabel.rx.text)
            .disposed(by: disposeBag)
        
        outputs
            .cameraAccessAuthorized
            .bind(to: accessCameraButton.rx.isHidden)
            .disposed(by: disposeBag)
        
        outputs
            .cameraAccessAuthorized
            .bind(to: cameraPermissionStackView.rx.isHidden)
            .disposed(by: disposeBag)
        
        outputs
            .cameraAccessAuthorized
            .map({!$0})
            .bind(to: qrCodeView.rx.isHidden)
            .disposed(by: disposeBag)
        
        outputs
            .cameraAccessAuthorized
            .filter({$0})
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { _ in
                self.captureSession.startRunning()
            }).disposed(by: disposeBag)
    
        
        logoutButton
            .rx
            .tap
            .subscribe(onNext: {_ in
                AuthManager.shared.logOut()
        }).disposed(by: disposeBag)
        
    }
    
    private func setupScanner() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera, .builtInTripleCamera, .builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back)

        guard let captureDevice = deviceDiscoverySession.devices.first else {
            print("Failed to get the camera device")
            return
        }

        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)

            // Set the input device on the capture session.
            captureSession.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            
            captureSession.addOutput(captureMetadataOutput)
            
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
             videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            
            guard let previewLayer = videoPreviewLayer else { return }
            
            previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            
            qrCodeView.layer.addSublayer(previewLayer)
            
            DispatchQueue.main.async {
                previewLayer.frame = self.qrCodeView.layer.bounds
                self.qrCodeView.bringSubviewToFront(self.qrImageView)
            }

        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
    }
    
        
    private func playScanSound() {
        guard let url = Bundle.main.url(forResource: "AppleSuccess", withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            audioPlayer = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            /* iOS 10 and earlier require the following line:
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */

            guard let player = audioPlayer else { return }

            player.play()

        } catch let error {
            print(error.localizedDescription)
        }
    }
}

extension CodeScanViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {

        guard let metadataObj = metadataObjects.first as? AVMetadataMachineReadableCodeObject else { return }

        if metadataObj.type == AVMetadataObject.ObjectType.qr {
            if let vendorScanData = metadataObj.stringValue  {
                let scannedComponents = vendorScanData.components(separatedBy: ",")
                
                let vendorId = scannedComponents[0]
                
                let locationId = scannedComponents[1].trimmingCharacters(in: .whitespacesAndNewlines)
                    
                let tableId = scannedComponents[2].trimmingCharacters(in: .whitespacesAndNewlines)
                
                let scanResult = FSScanResult(vendorId: vendorId, locationId: locationId, tableId: tableId)
                                
                scannedVendorData.onNext(scanResult)
            }
        }
    }
}


