//
//  VerificationCodeViewController.swift
//  FoodStuff
//
//  Created by Sebastian Nitu on 08.05.2021.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture

class VerificationCodeViewController: UIViewController, HasDisposeBag {

    @IBOutlet weak var verificationCodeTextField: UITextField!
    @IBOutlet weak var loginButton: LoadingButton!
    
    var viewModelFactory: (VerificationCodeViewModel.Inputs) -> VerificationCodeViewModel = { _ in fatalError("Must provide factory function first.") }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupReactiveExtensions()
    }
    
    override func viewDidLayoutSubviews() {
        verificationCodeTextField.layer.borderWidth = 2
        verificationCodeTextField.layer.borderColor = UIColor.textfieldBorder.cgColor
        verificationCodeTextField.layer.cornerRadius = 8

        loginButton.layer.cornerRadius = 8
        super.viewDidLayoutSubviews()
    }
    
    private func setupReactiveExtensions() {
        let inputs = VerificationCodeViewModel.Inputs(bag: disposeBag,
                                                      verificationCode: verificationCodeTextField.rx.text.asObservable(),
                                                      signInTapped: loginButton.rx.tap.asObservable())
        
        let viewModel = self.viewModelFactory(inputs)
        
        let signInButtonLoading = viewModel.outputs.signInButtonLoading

        let userInteractionEnabled = signInButtonLoading.map({!$0}).share(replay: 1) /// Used to restrict user interaction once login has been tapped.

        signInButtonLoading
            .bind(to: loginButton.rx.isLoading)
            .disposed(by: disposeBag)

        userInteractionEnabled
            .bind(to: loginButton.rx.isUserInteractionEnabled)
            .disposed(by: disposeBag)

        userInteractionEnabled
            .bind(to: verificationCodeTextField.rx.isUserInteractionEnabled)
            .disposed(by: disposeBag)

        viewModel
            .outputs
            .signInButtonEnabled
            .bind(to: loginButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        viewModel
            .outputs
            .signedIn
            .subscribe(onNext: { _ in
                AppRouter.shared.switchToScanner()
            }).disposed(by: disposeBag)

    }
}
