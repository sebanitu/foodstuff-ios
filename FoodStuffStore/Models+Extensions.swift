
extension FSOrder {
    func toStoreOrder() -> StoreOrder? {
        guard let storeOrderState = self.state.toStoreOrderState() else {
            return nil
        }
        return StoreOrder(
            orderId: self.orderId,
            user: self.user,
            tableNumber: self.tableNumber,
            orderTotal: self.orderTotal,
            state: storeOrderState,
            items: self.items.map { $0.toStoreOrderItem() }
        )
    }
}

extension FSOrderState {
    func toStoreOrderState() -> StoreOrderState? {
        StoreOrderState(rawValue: self.rawValue)
    }
}

extension FSOrderItem {
    func toStoreOrderItem() -> StoreOrderItem {
        StoreOrderItem(
            name: self.name,
            quantity: self.quantity,
            itemTotal: self.itemTotal,
            unitPrice: self.unitPrice
        )
    }
}
