
import Foundation

enum StoreOrderState: String {
    case received = "RECEIVED"
    case preparing = "PREPARING"
    case atTable = "AT_TABLE"
    case paid = "PAID"
}

struct StoreOrderItem: Hashable {
    let id = UUID()
    let name: String
    let quantity: Int
    let itemTotal: Double
    let unitPrice: Double
}

struct StoreOrder: Hashable {
    let orderId: String
    let user: String
    let tableNumber: Int
    let orderTotal: Double
    let state: StoreOrderState
    let items: [StoreOrderItem]
}
