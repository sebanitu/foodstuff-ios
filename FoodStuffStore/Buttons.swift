
import UIKit

// TODO: Mai trebuie schimbate `backgroundColor` la result butoanelor inafara de `OrderDetailsButton`
class OrderDetailsButton: UIButton {
    var onButtonTapped: (() -> Void)?

    convenience init() {
        self.init(frame: .zero)

        setup()
    }

    private func setup() {
        titleLabel?.font = .preferredFont(forTextStyle: .title1)
        setTitle("Vizualizează", for: .normal)
        backgroundColor = UIColor(named: "RedAccent")
        layer.cornerRadius = 6
        setTitleColor(.white, for: .normal)

        addTarget(self, action: #selector(onTap), for: .touchUpInside)
    }

    @objc
    func onTap() {
        onButtonTapped?()
    }
}

class InPreparationButton: UIButton {
    convenience init() {
        self.init(frame: .zero)

        setup()
    }

    private func setup() {
        titleLabel?.font = .preferredFont(forTextStyle: .title1)
        setTitle("În preparare", for: .normal)
        backgroundColor = .systemOrange
        layer.cornerRadius = 6
        setTitleColor(.white, for: .normal)
    }
}

class AtTableButton: UIButton {
    convenience init() {
        self.init(frame: .zero)

        setup()
    }

    private func setup() {
        titleLabel?.font = .preferredFont(forTextStyle: .title1)
        setTitle("La masa clientului", for: .normal)
        backgroundColor = .systemBlue
        layer.cornerRadius = 6
        setTitleColor(.white, for: .normal)
    }
}

class PaidButton: UIButton {
    convenience init() {
        self.init(frame: .zero)

        setup()
    }
    
    var onButtonTapped: (() -> Void)?
    
    @objc
    func onTap() {
        onButtonTapped?()
    }

    private func setup() {
        titleLabel?.font = .preferredFont(forTextStyle: .title1)
        setTitle("Plătit", for: .normal)
        backgroundColor = .systemGreen
        layer.cornerRadius = 6
        setTitleColor(.white, for: .normal)
        
        addTarget(self, action: #selector(onTap), for: .touchUpInside)
       
        
    
    }
}

