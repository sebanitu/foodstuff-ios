//
//  OrderAPI+Store.swift
//  FoodStuffStore
//
//  Created by Sebastian Nitu on 20.006.2021.
//

import Foundation
import RxSwift

class OrderAPI: NSObject {

    func observeLocationOrders(locationId: String, completion: @escaping (Result<[FSOrder], Error>) -> Void) {
        let ref = FSDatabaseReference.Orders.referenceForVendorOpenOrders(locationId).ref
        ref.removeAllObservers()

        ref.observe(.value) { snapshot in
            guard snapshot.exists(), let value = snapshot.value else {
                completion(.success([]))
                return
            }

            do {
                if let dict = value as? [String: [String: Any]] {
                    let orders = dict.map { $0.1 }
                    let decodedOrderData = try FSDecoder().decode([FSOrder].self, from: orders)
                    completion(.success(decodedOrderData))
                } else {
                    completion(.success([]))
                }
            } catch let error {
                print("⏰ Error: \(error.localizedDescription)")
                completion(.failure(error))
            }
        }
    }

    func updateOrderState(locationId: String, orderId: String, status: FSOrderState, completion: @escaping (Result<Void, Error>) -> Void) {
        guard let updateData = try? FSEncoder().encode(status) else {
            completion(.failure(LogError.GenericError.encodingFailure))
            return
        }

        FSDatabaseReference.Orders.referenceForSpecificOrder(orderId, locationId: locationId).ref
            .updateChildValues(["state": updateData]) { error,_ in
                if let error = error {
                    completion(.failure(LogError.User.failedCreationOfUserInDatabse(error)))
                    return
                }
                completion(.success(()))
            }
    }
    
    private func removeOrder(order: FSOrder, completion: @escaping (Result<Void, Error>) -> Void) {
        
        guard let encodedOrder = try? FSEncoder().encode(order) else {
            return
        }
        
        FSDatabaseReference.Orders.referenceForVendorArchivedOrders(order.locationId).ref.child(order.orderId).setValue(encodedOrder) { error, _ in
            if let error = error {
                completion(.failure(error))
                return
            }
            completion(.success(()))
        }
    }
    
    private func removeUserOrder(order: FSOrder, completion: @escaping (Result<Void, Error>) -> Void) {
        FSDatabaseReference.User.openOrderReferenceForUserWithId(order.userId).ref.removeValue()
            completion(.success(()))
        }
    
    func archiveOrder(order: FSOrder, completion: @escaping (Result<Void, Error>) -> Void) {
        removeOrder(order: order) { result in
            switch result {
            case .success(()):
                self.removeUserOrder(order: order) { result in
                    switch result {
                    case .success(()): FSDatabaseReference.Orders.referenceForSpecificOrder(order.orderId, locationId: order.locationId).ref
                        .removeValue()
                        completion(.success(()))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

}


extension Reactive where Base: OrderAPI {

    func observeLocationOrders(locationId: String) -> Observable<[FSOrder]> {
        return Observable.create { observer in
            base.observeLocationOrders(locationId: locationId) { result in
                switch result {
                    case .success(let orders):
                        observer.onNext(orders)
                        observer.onCompleted()

                    case .failure(let error):
                        observer.onError(error)
                }
            }
            return Disposables.create()
        }
    }
    
   

}
