//
//  ViewController.swift
//  FoodStuffStore
//
//  Created by Sebastian Nitu on 21.06.2021.
//

import UIKit

// TODO: Am facut .read si .write fara auth ca sa fie mai usor
// TODO: De gasit o metoda sa pastrezi ordinea celulelor
class ViewController: UICollectionViewController {

    private enum Section {
        case main
    }

    private var dataSource: UICollectionViewDiffableDataSource<Section, FSOrder>?

    static let pizzaCodleaStoreID = "9421eb7a-77c7-4c77-a5a6-bdf4ac7af638"

    override func viewDidLoad() {
        super.viewDidLoad()

        configureCollectionView()

        OrderAPI().observeLocationOrders(locationId: Self.pizzaCodleaStoreID) { [weak self] result in
            if let storeOrders = try? result.get().compactMap({ $0 }) {
                self?.createSnapshot(orders: storeOrders)
            }
        }
    }

    private func configureCollectionView() {
        collectionView.setCollectionViewLayout(generateLayout(), animated: true)
        configureDataSource()
    }

    private func configureDataSource() {
        dataSource = UICollectionViewDiffableDataSource<Section, FSOrder>(collectionView: collectionView) {
            [weak self] (collectionView: UICollectionView, indexPath: IndexPath, order: FSOrder) -> UICollectionViewCell? in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "orderCell", for: indexPath) as! OrderCollectionViewCell
            let viewModel = OrderCellViewModel(order: order, orderState: order.state)
            viewModel.onDetailsButtonTapped = {
                self?.present(OrderDetailsViewController.makeOrderDetails(order: order), animated: true)
            }
            
            viewModel.onPaidButtonTapped = {
                OrderAPI().archiveOrder(order: order) { result in
                    switch result {
                    case .success(()):
                        print("Order \(order.orderId) has been archived and removed from user")
                        return
                    default : return
                    }
                }
            }
            
            cell.viewModel = viewModel
            return cell
        }
    }

    private func createSnapshot(orders: [FSOrder]) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, FSOrder>()

        snapshot.appendSections([.main])
        snapshot.appendItems(orders)

        self.dataSource?.apply(snapshot, animatingDifferences: true)
    }

    private func generateLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.2),
                                              heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10)

        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                               heightDimension: .fractionalWidth(0.3))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 3)

        let section = NSCollectionLayoutSection(group: group)

        let layout = UICollectionViewCompositionalLayout(section: section)

        return layout
    }

}
