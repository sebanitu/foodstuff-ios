
import UIKit

class OrderCellViewModel {

    let tableNumber: Int
    let orderState: FSOrderState

    var onDetailsButtonTapped: (() -> Void)?
    var onPaidButtonTapped: (() -> Void)?

    init(order: FSOrder, orderState: FSOrderState, onDetailsButtonTapped: (() -> Void)? = nil, onPaidButtonTapped: (() -> Void)? = nil) {
        self.tableNumber = order.tableNumber
        self.orderState = orderState
        self.onDetailsButtonTapped = onDetailsButtonTapped
        self.onPaidButtonTapped = onPaidButtonTapped
    }

    var buttons: [UIButton] {
        switch orderState {
            case .received:
                return [OrderDetailsButton()]

            case .preparing:
                return [InPreparationButton(), OrderDetailsButton()]

            case .atTable:
                return [AtTableButton(), OrderDetailsButton()]

            case .paid:
                return [PaidButton()]
        }
    }

}
