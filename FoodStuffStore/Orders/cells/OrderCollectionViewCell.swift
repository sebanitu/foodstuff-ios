
import UIKit

class OrderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableNumber: UILabel!
    @IBOutlet weak var buttonsStack: UIStackView!

    var viewModel: OrderCellViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            tableNumber.text = "Masa \(viewModel.tableNumber)"
            viewModel.buttons.forEach { button in
                buttonsStack.addArrangedSubview(button)

                (button as? OrderDetailsButton)?.onButtonTapped = viewModel.onDetailsButtonTapped
                (button as? PaidButton)?.onButtonTapped = viewModel.onPaidButtonTapped

                
                button.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
            }
        }
    }

    override func prepareForReuse() {
        buttonsStack.arrangedSubviews.forEach { view in
            buttonsStack.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
        super.prepareForReuse()
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        setupShadow()

        containerView.layer.cornerRadius = 6
    }

    private func setupShadow() {
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.2
        containerView.layer.shadowOffset = CGSize(width: 2, height: 2)
        containerView.layer.shadowRadius = 6

        containerView.layer.shouldRasterize = true
        containerView.layer.rasterizationScale = UIScreen.main.scale
    }

}
