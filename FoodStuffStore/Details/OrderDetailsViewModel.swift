
class OrderDetailsViewModel {

    let order: FSOrder

    var totalText: String {
        "TOTAL: \(order.orderTotal) LEI"
    }

    var buttonTitle: String {
        switch order.state {
            case .received:
                return "Trimite la bucătărie"

            case .preparing:
                return "Trimite la masă"

            case .atTable:
                return "Încheie comandă"

            case .paid:
                return "Plătit"
        }
    }

    var showError: (() -> Void)?
    var dismiss: (() -> Void)?

    init(order: FSOrder) {
        self.order = order
    }

    func updateOrderStatus() {
        switch order.state {
            case .received:
                updateOrderState(.preparing)

            case .preparing:
                updateOrderState(.atTable)

            case .atTable:
                updateOrderState(.paid)

            case .paid:
                break
        }
    }

    private func updateOrderState(_ state: FSOrderState) {
        OrderAPI().updateOrderState(
            locationId: ViewController.pizzaCodleaStoreID,
            orderId: order.orderId,
            status: state
        ) { [weak self] result in
            if (try? result.get()) == nil {
                self?.showError?()
            } else {
                self?.dismiss?()
            }
        }
    }

}
