
import UIKit

class OrderItemTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var itemLabel: UILabel!

    override func prepareForReuse() {
        itemLabel.text = nil
        super.prepareForReuse()
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        setupShadow()

        containerView.layer.cornerRadius = 6
    }

    private func setupShadow() {
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.2
        containerView.layer.shadowOffset = CGSize(width: 2, height: 2)
        containerView.layer.shadowRadius = 6

        containerView.layer.shouldRasterize = true
        containerView.layer.rasterizationScale = UIScreen.main.scale
    }

}
