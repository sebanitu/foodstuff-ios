
struct OrderItemViewModel {

    let orderItem: FSOrderItem

    var displayText: String {
        "\(orderItem.quantity) x \(orderItem.name) -- \(orderItem.itemTotal) LEI"
    }

}
