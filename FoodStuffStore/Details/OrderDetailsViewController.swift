
import UIKit

class OrderDetailsViewController: UIViewController {

    enum Section {
        case main
    }

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!

    var viewModel: OrderDetailsViewModel?
    var dataSource: UITableViewDiffableDataSource<Section, FSOrderItem>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureDataSource()

        createSnapshot()

        bindViewModel()
    }

    private func bindViewModel() {
        totalLabel.text = viewModel?.totalText
        actionButton.setTitle(viewModel?.buttonTitle, for: .normal)
        actionButton.addTarget(self, action: #selector(updateOrderStatus), for: .touchUpInside)

        viewModel?.showError = { [weak self] in
            self?.present(Self.makeAlertErrorController(), animated: true)
        }
        viewModel?.dismiss = { [weak self] in
            self?.dismiss(animated: true)
        }
    }

    private static func makeAlertErrorController() -> UIAlertController {
        let alert = UIAlertController(title: "Eroare", message: "Mai incearcă", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            alert.dismiss(animated: true)
        }))
        return alert
    }

    private func configureDataSource() {
        dataSource = UITableViewDiffableDataSource<Section, FSOrderItem>(tableView: tableView) { tableView, indexPath, orderItem in
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderItemTableViewCell") as! OrderItemTableViewCell
            let cellViewModel = OrderItemViewModel(orderItem: orderItem)
            cell.itemLabel.text = cellViewModel.displayText
            return cell
        }
    }

    private func createSnapshot() {
        guard let items = viewModel?.order.items else {
            return
        }

        var snapshot = NSDiffableDataSourceSnapshot<Section, FSOrderItem>()

        snapshot.appendSections([.main])
        snapshot.appendItems(items)

        dataSource?.apply(snapshot, animatingDifferences: true)
    }

    @objc
    private func updateOrderStatus() {
        viewModel?.updateOrderStatus()
    }

}

// MARK: - `OrderDetailsViewController` factory method
extension OrderDetailsViewController {

    static func makeOrderDetails(order: FSOrder) -> OrderDetailsViewController {
        let orderDetailsViewModel = OrderDetailsViewModel(order: order)
        let orderDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "OrderDetailsViewController") as! OrderDetailsViewController
        orderDetailsViewController.viewModel = orderDetailsViewModel

        return orderDetailsViewController
    }

}
